# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2.0.9083] - 2024-11-13 Preview 202304
* Update packages.
* Upgrade Forms libraries and Test and Demo apps to .Net 9.0.
* Increment version to 9083.

## [2.0.8956] - 2024-07-09 Preview 202304
* Set projects to target .NET Standard 2.0 alone.
* Update .NET standard 2.0 enhanced methods and attributes.

## [2.0.8943] - 2024-06-26 Preview 202304
* Update to .Net 8.
* Update libraries versions.
* Apply code analysis rules.
* Use ordinal instead of ordinal ignore case string comparisons.
* Split off the test settings singletons for the test site settings and default instances.
* Apply code analysis rules.
* Generate assembly attributes.

## [2.0.8935] - 2024-06-17 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.

## [2.0.8536] - 2023-05-16 Preview 202304
* Rearrange the MS Test project folders. 
* Add singlton instance tot he test site settings and test.
* Update packages.

## [2.0.8533] - 2023-05-13 Preview 202304
* Fix object binding. 
* Add logging and serilog settings classes and unit tests.

## [2.0.8531] - 2023-05-11 Preview 202304
* Replace configuration binding with Json serialization on deep object binding.
* Add Assembly File Info and Settings Scriber to handle reading and writing of the settings. 
* Add methods for saving and restoring individual settings classes.

## [2.0.8528] - 2023-05-08 Preview 202304
* Remove legacy Settings libraries.
* Implements the Model-View-View-Model pattern.
* Enable settings sections for settings.

## [1.1.8518] - 2023-04-28 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.
* App Settings:
  * Use Lazy singleton instance. Correct the construction of the default settings file names.

## [1.0.8517] - 2023-04-28 Preview 202304
* App Settings:
  * Add MS Test, Windows Forms and Windows Forms dmeo projects.

## [1.0.8502] - 2023-04-12
* Change namespace to cc.isr.Json. Update settings.

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8118] - 2022-03-24
* Add initialization method for saving the initial application settings.
* Add restorable condition based on the existence of the application context
settings file. 
* Add demo for demonstrating how to address the absence of a 
application context settings file where the settings file needs to be 
created when the settings class is initialized but only if the 
application settings file does not exist.
* add application settings base class for settings with multiple sections.
* modify the settings form and control for handling application settings 
with multiple sections.

## [1.0.8109] - 2022-03-15
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8103] - 2022-03-09
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[2.0.9083]: https://bitbucket.org/davidhary/dn.json/Preview202304
