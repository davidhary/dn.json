# Open Source

Support libraries extending the Visual Studio framework for using Json.
This is a fork of the [dn.core] repository.

* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Open-Source"></a>
## Open Source
Open source used by this software is described and licensed at the
following sites:  
[json]  
[Drop Shadow and Fade From]  
[Exception Extension]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  


[json]: https://www.bitbucket.org/davidhary/dn.json
[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[Drop Shadow and Fade From]: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc

