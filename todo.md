### TODO

#### MVVM
* Add MAUI forms and demo projects;
* Add UNO forms and demo projects;
* Replace Notify Propery Change calls with Set Property

#### Package

* Add the project to GIT hub repository.
* Add a GitHub folder with action to build the WinForms package.
* Use GIT hub actions to unit test and build the package.
