using Win32;

namespace cc.isr.Json.AppSettings.WinForms.Demo;

internal static class ExceptionHandlers
{
    /// <summary>   Reports unhandled exception event. </summary>
    /// <remarks>   2023-05-01. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    public static void OnUnhandledException( object? sender, UnhandledExceptionEventArgs e )
    {
        _ = NativeMethods.AllocateConsole();
        Console.WriteLine( $"\nUnhandled exception occurred: {e.ExceptionObject}\n" );
        _ = Console.ReadKey();
    }

    /// <summary>   Reports unobserved task exception events. </summary>
    /// <remarks>   2023-05-01. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information to send to registered event handlers. </param>
    public static void OnTaskSchedulerUnobservedException( object? sender, UnobservedTaskExceptionEventArgs e )
    {
        _ = NativeMethods.AllocateConsole();
        Console.WriteLine( $"\n{(e.Observed ? "" : "un")}observed exception occurred: {e.Exception}\n" );
        _ = Console.ReadKey();
    }
}
