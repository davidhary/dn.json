using System.CommandLine.Builder;
using System.CommandLine;
using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.ViewModels;

namespace cc.isr.Json.AppSettings.WinForms.Demo;

/// <summary>   A class for constructing a custom command line parser. </summary>
/// <remarks>   2023-04-26. </remarks>
internal sealed class CommandLineParser : Command
{
    /// <summary>   Query if the command line arguments <paramref name="args"/> tell the program
    ///             to output to a console. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="args"> The arguments. </param>
    /// <returns>   True if console out, false if not. </returns>
    public static bool IsConsoleOut( string[] args )
    {
        return args is not null && args.Length > 0 && ContainsAny( args, ["-?", "-h", "--help", "--version"] );
    }

    /// <summary>   Query if <paramref name="args"/> contains any one of the specified values. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="args">     The arguments. </param>
    /// <param name="values">   The values. </param>
    /// <returns>   True if contains, false if not. </returns>
    public static bool ContainsAny( string[] args, string[] values )
    {
        foreach ( string item in values )
        {
            if ( args.Contains( item ) )
                return true;
        }
        return false;
    }

    /// <summary>   Gets the parser. </summary>
    /// <value> The parser. </value>
    public System.CommandLine.Parsing.Parser Parser { get; }

    public CommandLineParser() : base( "AppSettingsEditor", "Demonstration of the application settings editor" )
    {
        Option<EditorOption> editorOption = new(
            "--editor",
            description: "Specifies the editor option",
            getDefaultValue: () => EditorOption.CustomForm );

        Option<ScopeOption> scopeOption = new(
            "--scope",
            description: "Specifies the scope option",
            getDefaultValue: () => ScopeOption.ThisUser );

        this.AddOption( editorOption );
        this.AddOption( scopeOption );

        this.SetHandler( ( editor, scope ) =>
        {
            _ = Application.SetHighDpiMode( HighDpiMode.SystemAware );
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );

            IServiceProvider serviceProvider = SimpleServiceProvider.GetInstance();

            // use the scope to select the active application settings file.

            AssemblyFileInfo assemblyFileInfo = new( typeof( Program ).Assembly );

            string scopeFile = scope == ScopeOption.AppContext
                ? assemblyFileInfo.AppContextAssemblyFilePath!
                : scope == ScopeOption.ThisUser
                    ? assemblyFileInfo.ThisUserAssemblyFilePath!
                    : assemblyFileInfo.AllUsersAssemblyFilePath!;


            // build the app settings source model for the View Model

            AppSettingsScribe model = new( [new Settings(), new TestSettings()],
                                                         assemblyFileInfo.AppContextAssemblyFilePath!,
                                                         scopeFile );

            // build the view model with the model and service provider

            AppSettingsEditorViewModel viewModel = new( model, serviceProvider );

            Form form = new();

            if ( editor == EditorOption.CustomForm )
            {
                form = new JsonSettingsEditorForm( "Settings editor demo", viewModel );
            }
            else
            {
                JsonSettingsEditorControl control = new()
                {
                    SettingsEditorViewModel = viewModel
                };

                // add the control to a UI form.

                form = new()
                {
                    Text = "Settings editor control demo",
                    ClientSize = new Size( 427, 359 )
                };
                form.Controls.Add( control );
                control.Dock = DockStyle.Fill;
            }

            // run the application 

            Application.Run( form );

        },
            editorOption, scopeOption );

        System.CommandLine.Parsing.Parser parser = new CommandLineBuilder( this )
            .UseDefaults()
            .UseHelp( ctx =>
            {
                ctx.HelpBuilder.CustomizeSymbol( editorOption,
                    firstColumnText: ( ctx ) => $"--editor <{EditorOption.CustomForm}, or {EditorOption.CustomControl}>",
                    secondColumnText: ( ctx ) => "Specifies the editor option. " +
                        "Choose a custom form with an embedded property grid " +
                        "Or a standard form with a custom property grid control." );
                ctx.HelpBuilder.CustomizeSymbol( scopeOption,
                    firstColumnText: ( ctx ) => $"--scope <{ScopeOption.AppContext}|{ScopeOption.ThisUser}|{ScopeOption.AllUsers}>",
                    secondColumnText: ( ctx ) => "Chose the application settings file for " +
                        "the application, this user or all users scope." );
            } )
            .Build();
        this.Parser = parser;
    }

    /// <summary>   Values that represent editor options. </summary>
    /// <remarks>   2023-04-26. </remarks>
    public enum EditorOption
    {
        CustomForm, CustomControl
    }

    public enum ScopeOption
    {
        AppContext, ThisUser, AllUsers
    }

}
