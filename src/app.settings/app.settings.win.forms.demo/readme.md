# About

[cc.isr.Json.AppSettings.WinForms.Demo] is a .Net Windows Forms application
demonstrating reading, writing and resetting configuration 
settings using Json.

# How to Use

* The settings file is read upon launching the program.
* The settings file can then be edited and saved or restored from 
the original 'context' settings.
* Command line help is displayed using the `-h` command line argument.

# Running from the IDE

* select an option from the run drop down:
    * Help - displays the command line help information;
    * Version - displays the program version.
    * Property Grid Control [ App Context / user/  all users ]: opens the settings editor control with its drop down menu editing the application context (e.g., c:\Program Data settings), All users or this user settings;
    * Property Grid Form [ App Context / user/  all users ]: opens the settings editor form with the editor with its drop down menu editing the application context (e.g., c:\Program Data settings), All users or this user settings;

# Key Features

* Demonstrates editing of Json-based configuration settings files.
* Assumes that a settings file already exists in the application folder.
* Displays version information and help as a console application.
# Feedback

[cc.isr.Json.AppSettings.WinForms.Demo] is released as open source under the MIT license. Bug reports and contributions are welcome at the [Json Repository].

[Json Repository]: https://bitbucket.org/davidhary/dn.json
[cc.isr.Json.AppSettings.WinForms.Demo]: https://bitbucket.org/davidhary/dn.json/src/main/src/app.settings/app.settings.win.forms.demo/
