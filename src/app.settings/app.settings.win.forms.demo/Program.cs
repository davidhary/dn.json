using System.CommandLine;
using System.CommandLine.Parsing;
using cc.isr.Json.AppSettings.WinForms.Demo;
using Win32;

System.AppDomain.CurrentDomain.UnhandledException += ExceptionHandlers.OnUnhandledException;

// this handles exceptions that are propagated by relay commands to the Task Scheduler Unobserved Exception handler.

TaskScheduler.UnobservedTaskException += ExceptionHandlers.OnTaskSchedulerUnobservedException;

if ( CommandLineParser.IsConsoleOut( args ) )
{
    // allocates a console to receive the program console out lines. 
    _ = NativeMethods.AllocateConsole();
    _ = new CommandLineParser().Parser.Invoke( args );
    _ = Console.ReadKey();
}
else
    _ = new CommandLineParser().Parser.Invoke( args );


