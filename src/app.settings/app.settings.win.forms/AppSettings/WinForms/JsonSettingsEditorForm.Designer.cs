using System.Diagnostics;

namespace cc.isr.Json.AppSettings.WinForms;

public partial class JsonSettingsEditorForm
{
    // Required by the Windows Form Designer
    private System.ComponentModel.IContainer components;

    // NOTE: The following procedure is required by the Windows Form Designer
    // It can be modified using the Windows Form Designer.  
    // Do not modify it using the code editor.
    [DebuggerStepThrough()]
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this.PropertyGrid = new PropertyGrid();
        this.appSettingsEditorViewModelBindingSource = new BindingSource( this.components );
        this.MenuStrip = new MenuStrip();
        this.FileMenuItem = new ToolStripMenuItem();
        this.ResetMenuItem = new ToolStripMenuItem();
        this.ReadMenuItem = new ToolStripMenuItem();
        this.SaveMenuItem = new ToolStripMenuItem();
        this.AvailableSettingsCombo = new ToolStripComboBox();
        this.SaveExitMenuItem = new ToolStripMenuItem();
        this.FileSeparator1 = new ToolStripSeparator();
        this.ExitMenuItem = new ToolStripMenuItem();
        this.OkayButton = new Button();
        this.IgnoreButton = new Button();
        ((System.ComponentModel.ISupportInitialize) this.appSettingsEditorViewModelBindingSource).BeginInit();
        this.MenuStrip.SuspendLayout();
        this.SuspendLayout();
        // 
        // PropertyGrid
        // 
        this.PropertyGrid.Dock = DockStyle.Fill;
        this.PropertyGrid.Location = new Point( 0, 24 );
        this.PropertyGrid.Margin = new Padding( 3, 2, 3, 2 );
        this.PropertyGrid.Name = "PropertyGrid";
        this.PropertyGrid.Size = new Size( 427, 335 );
        this.PropertyGrid.TabIndex = 0;
        // 
        // appSettingsEditorViewModelBindingSource
        // 
        this.appSettingsEditorViewModelBindingSource.DataSource = typeof( ViewModels.AppSettingsEditorViewModel );
        // 
        // MenuStrip
        // 
        this.MenuStrip.Items.AddRange( new ToolStripItem[] { this.FileMenuItem } );
        this.MenuStrip.Location = new Point( 0, 0 );
        this.MenuStrip.Name = "MenuStrip";
        this.MenuStrip.Size = new Size( 427, 24 );
        this.MenuStrip.TabIndex = 1;
        this.MenuStrip.Text = "Menu Strip";
        // 
        // FileMenuItem
        // 
        this.FileMenuItem.DropDownItems.AddRange( new ToolStripItem[] { this.ResetMenuItem, this.ReadMenuItem, this.SaveMenuItem, this.AvailableSettingsCombo, this.SaveExitMenuItem, this.FileSeparator1, this.ExitMenuItem } );
        this.FileMenuItem.Name = "FileMenuItem";
        this.FileMenuItem.Size = new Size( 37, 20 );
        this.FileMenuItem.Text = "&File";
        // 
        // ResetMenuItem
        // 
#if NET8_0_OR_GREATER
        this.ResetMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "RestoreCommand", true ) );
#endif
        this.ResetMenuItem.Name = "ResetMenuItem";
        this.ResetMenuItem.Size = new Size( 181, 22 );
        this.ResetMenuItem.Text = "R&eset";
        this.ResetMenuItem.ToolTipText = "Restores the persisted application settings values to their corresponding default properties";
        // 
        // ReadMenuItem
        // 
#if NET8_0_OR_GREATER
        this.ReadMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "ReadCommand", true ) );
#endif
        this.ReadMenuItem.Name = "ReadMenuItem";
        this.ReadMenuItem.Size = new Size( 181, 22 );
        this.ReadMenuItem.Text = "&Read";
        this.ReadMenuItem.ToolTipText = "Read the settings from the application settings folder";
        // 
        // SaveMenuItem
        // 
#if NET8_0_OR_GREATER
        this.SaveMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "WriteCommand", true ) );
#endif
        this.SaveMenuItem.Name = "SaveMenuItem";
        this.SaveMenuItem.Size = new Size( 181, 22 );
        this.SaveMenuItem.Text = "&Save";
        this.SaveMenuItem.ToolTipText = "Write the settings to the application  settings folder";
        // 
        // AvailableSettingsCombo
        // 
        this.AvailableSettingsCombo.Items.AddRange( new object[] { "a", "b" } );
        this.AvailableSettingsCombo.Name = "AvailableSettingsCombo";
        this.AvailableSettingsCombo.Size = new Size( 121, 23 );
        this.AvailableSettingsCombo.ToolTipText = "Select a settings from the list";
        // 
        // SaveExitMenuItem
        // 
        this.SaveExitMenuItem.Name = "SaveExitMenuItem";
        this.SaveExitMenuItem.Size = new Size( 181, 22 );
        this.SaveExitMenuItem.Text = "S&ave and Exit";
        this.SaveExitMenuItem.ToolTipText = "Save and exit";
        this.SaveExitMenuItem.Click += this.SaveExitMenuItem_Click;
        // 
        // FileSeparator1
        // 
        this.FileSeparator1.Name = "FileSeparator1";
        this.FileSeparator1.Size = new Size( 178, 6 );
        // 
        // ExitMenuItem
        // 
        this.ExitMenuItem.Name = "ExitMenuItem";
        this.ExitMenuItem.Size = new Size( 181, 22 );
        this.ExitMenuItem.Text = "E&xit";
        this.ExitMenuItem.ToolTipText = "Close the form.";
        this.ExitMenuItem.Click += this.ExitMenuItem_Click;
        // 
        // OkayButton
        // 
        this.OkayButton.DialogResult = DialogResult.Cancel;
        this.OkayButton.Location = new Point( 304, 69 );
        this.OkayButton.Margin = new Padding( 3, 2, 3, 2 );
        this.OkayButton.Name = "OkayButton";
        this.OkayButton.Size = new Size( 75, 30 );
        this.OkayButton.TabIndex = 2;
        this.OkayButton.Text = "Okay";
        this.OkayButton.UseVisualStyleBackColor = true;
        this.OkayButton.Click += this.AcceptButton_Click;
        // 
        // IgnoreButton
        // 
        this.IgnoreButton.DialogResult = DialogResult.Cancel;
        this.IgnoreButton.Location = new Point( 304, 69 );
        this.IgnoreButton.Margin = new Padding( 3, 2, 3, 2 );
        this.IgnoreButton.Name = "IgnoreButton";
        this.IgnoreButton.Size = new Size( 75, 30 );
        this.IgnoreButton.TabIndex = 2;
        this.IgnoreButton.Text = "Cancel";
        this.IgnoreButton.UseVisualStyleBackColor = true;
        this.IgnoreButton.Click += this.CancelButton_Click;
        // 
        // JsonSettingsEditorForm
        // 
        this.AcceptButton = this.OkayButton;
        this.AutoScaleDimensions = new SizeF( 7F, 15F );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.CancelButton = this.IgnoreButton;
        this.ClientSize = new Size( 427, 359 );
        this.Controls.Add( this.PropertyGrid );
        this.Controls.Add( this.MenuStrip );
        this.Controls.Add( this.OkayButton );
        this.Controls.Add( this.IgnoreButton );
        this.Font = new Font( "Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point );
        this.MainMenuStrip = this.MenuStrip;
        this.Margin = new Padding( 3, 2, 3, 2 );
        this.Name = "JsonSettingsEditorForm";
        this.Text = "Json Settings Editor";
        ((System.ComponentModel.ISupportInitialize) this.appSettingsEditorViewModelBindingSource).EndInit();
        this.MenuStrip.ResumeLayout( false );
        this.MenuStrip.PerformLayout();
        this.ResumeLayout( false );
        this.PerformLayout();
    }

    private System.Windows.Forms.PropertyGrid PropertyGrid;
    private System.Windows.Forms.MenuStrip MenuStrip;
    private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ReadMenuItem;
    private System.Windows.Forms.ToolStripSeparator FileSeparator1;
    private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
    private System.Windows.Forms.Button OkayButton;
    private System.Windows.Forms.Button IgnoreButton;
    private System.Windows.Forms.ToolStripMenuItem SaveMenuItem;
    private System.Windows.Forms.ToolStripMenuItem SaveExitMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ResetMenuItem;
    private ToolStripComboBox AvailableSettingsCombo;
    private BindingSource appSettingsEditorViewModelBindingSource;
}
