using System.Collections;
using System.ComponentModel;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.Services.Extensions;
using cc.isr.Json.AppSettings.ViewModels;

namespace cc.isr.Json.AppSettings.WinForms;

/// <summary>   A JSON configuration settings editor control. </summary>
/// <remarks>   David, 2021-07-27. </remarks>
public partial class JsonSettingsEditorControl : UserControl
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-07-27. </remarks>
    public JsonSettingsEditorControl()
    {
        this.InitializeComponent();

        // We need a marshalling control to asynchronously invoke.

        WinFormsDialogService.RegisterWinFormsAsyncHelper( WinFormsAsyncHelper.GetInstance( this ) );

        // register the win forms dialog service.

        _ = SimpleServiceProvider.GetInstance().GetRequiredService<WinFormsDialogService>();

#if NET8_0_OR_GREATER
        this.DataContextChanged += this.HandleDataContextChanged;
#endif
    }

    /// <summary>   Constructor using a <see cref="cc.isr.Json.AppSettings.Services.SimpleServiceProvider"/>. </summary>
    /// <remarks>   2024-07-18. </remarks>
    /// <param name="model">    The model. </param>
    [CLSCompliant( false )]
    public JsonSettingsEditorControl( cc.isr.Json.AppSettings.Models.AppSettingsScribe model )
        : this() => this.SettingsEditorViewModel = new AppSettingsEditorViewModel( model,
                                cc.isr.Json.AppSettings.Services.SimpleServiceProvider.GetInstance() );

    /// <summary>   Handles the data context changed. </summary>
    /// <remarks>   2024-07-18. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void HandleDataContextChanged( object? sender, EventArgs e )
    {
#if NET8_0_OR_GREATER
        this.appSettingsEditorViewModelBindingSource.DataSource = this.DataContext;
        this.PropertyGrid.DataContext = this.DataContext;
#endif
    }

    #endregion

    #region " view model "

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0032:Use auto property", Justification = "<Pending>" )]
    private AppSettingsEditorViewModel? _settingsEditorViewModel;

    /// <summary>   Gets or sets the application settings editor view model. </summary>
    /// <value> The application settings editor view model. </value>
    [CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public AppSettingsEditorViewModel? SettingsEditorViewModel
    {
        get => this._settingsEditorViewModel;
        set
        {
            this._settingsEditorViewModel = value;
#if NET8_0_OR_GREATER
            this.DataContext = value;
#endif
            this.SetAppSettingsScribe( value?.Scribe );
            this.BindPropertyGrid();
        }
    }

    /// <summary>   Gets or sets the application settings editor view model. </summary>
    /// <value> The application settings editor view model. </value>
    [CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public AppSettingsScribe? Scribe { get; private set; }

    /// <summary>   Sets the application settings reader/writer. </summary>
    /// <param name="value"> The application settings source. </param>
    private void SetAppSettingsScribe( AppSettingsScribe? value )
    {
        if ( this.Scribe is not null )
            this.AvailableSettingsCombo.SelectedIndexChanged -= this.AvailableSettingsCombo_SelectedIndexChanged;

        this.Scribe = value;
        if ( this.Scribe is null )
        {
            this.ResetMenuItem.Enabled = false;
            this.ReadMenuItem.Enabled = false;
            this.SaveMenuItem.Enabled = false;
            this.AvailableSettingsCombo.Enabled = false;
        }
        else
        {
            this.AvailableSettingsCombo.Enabled = this.Scribe.SettingsDictionary.Count > 0;
            this.AvailableSettingsCombo.ComboBox.Items.Clear();
            this.AvailableSettingsCombo.ComboBox.DataSource = this.Scribe.SettingsDictionary.ToList();
            this.AvailableSettingsCombo.ComboBox.BindingContext = this.BindingContext;
            this.AvailableSettingsCombo.ComboBox.DisplayMember = nameof( DictionaryEntry.Key );
            this.AvailableSettingsCombo.ComboBox.ValueMember = nameof( DictionaryEntry.Value );
            this.AvailableSettingsCombo.SelectedIndexChanged += this.AvailableSettingsCombo_SelectedIndexChanged;
            this.Scribe.SelectSettings( this.Scribe.SelectedSection );
        }
    }

    #endregion

    #region " property grid binding "

    /// <summary>   Bind property grid. </summary>
    /// <remarks>
    /// 2023-05-02. <para>
    /// While the property grid selected property can be properly updated by direct settings, with
    /// binding the grid needs to explicitly be refreshed for the property change to take effect.
    /// This method invokes the property grid refresh method on the property grid thread to do the
    /// update.
    /// </para>
    /// </remarks>
    private void BindPropertyGrid()
    {
        this.PropertyGrid.DataBindings.Add( new Binding( nameof( System.Windows.Forms.PropertyGrid.SelectedObject ),
            this.appSettingsEditorViewModelBindingSource, nameof( ViewModels.AppSettingsEditorViewModel.SelectedSettings ), true,
            DataSourceUpdateMode.OnPropertyChanged ) );
        this.SettingsEditorViewModel!.PropertyChanged += ( object? sender, PropertyChangedEventArgs e ) => this.PropertyGrid.Invoke( () => this.PropertyGrid.Refresh() );
    }

    #endregion

    #region " ui handlers "

    /// <summary>
    /// Event handler. Called by AvailableSettingsCombo for selected index changed events.
    /// </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void AvailableSettingsCombo_SelectedIndexChanged( object? sender, EventArgs e )
    {
        try
        {
            this._settingsEditorViewModel?.SelectSettingsCommand.Execute( this.AvailableSettingsCombo.Text );
        }
        catch ( Exception ex )
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
            _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Resetting Settings", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
        }
        finally
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }

    #endregion
}
