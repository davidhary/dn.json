using System.Collections;
using System.ComponentModel;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.Services.Extensions;
using cc.isr.Json.AppSettings.ViewModels;

namespace cc.isr.Json.AppSettings.WinForms;

/// <summary> An editor form for JSON configuration settings. </summary>
/// <remarks> David, 2016-03-31. </remarks>
public partial class JsonSettingsEditorForm : Form
{
    #region " construction and cleanup "

    /// <summary>
    /// Prevents a default instance of the <see cref="JsonSettingsEditorForm" /> class from being
    /// created.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    private JsonSettingsEditorForm() : base()
    {
        // This method is required by the Windows Form Designer.
        this.InitializeComponent();

        // We need a marshalling control to asynchronously invoke.

        WinFormsDialogService.RegisterWinFormsAsyncHelper( WinFormsAsyncHelper.GetInstance( this ) );

        // register the win forms dialog service.

        _ = SimpleServiceProvider.GetInstance().GetRequiredService<WinFormsDialogService>();

#if NET8_0_OR_GREATER
        this.DataContextChanged += this.HandleDataContextChanged;
#endif
    }

    /// <summary>
    /// Prevents a default instance of the <see cref="JsonSettingsEditorForm" /> class from being
    /// created.
    /// </summary>
    /// <remarks>   David, 2022-03-24. </remarks>
    /// <param name="caption">                      The caption. </param>
    /// <param name="appSettingsEditorViewModel">   The application settings editor view model. </param>
    [CLSCompliant( false )]
    public JsonSettingsEditorForm( string caption, AppSettingsEditorViewModel appSettingsEditorViewModel ) : this()
    {
        this.Text = caption;
        this.SettingsEditorViewModel = appSettingsEditorViewModel;
    }

    private void HandleDataContextChanged( object? sender, EventArgs e )
    {
#if NET8_0_OR_GREATER
        this.appSettingsEditorViewModelBindingSource.DataSource = this.DataContext;
        this.PropertyGrid.DataContext = this.DataContext;
#endif
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="disposing"> <c>true</c> to release both managed and unmanaged resources;
    /// <c>false</c> to release only unmanaged
    /// resources when called from the runtime
    /// finalize. </param>
    [System.Diagnostics.DebuggerNonUserCode()]
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this.components = null;
            }
        }
        finally
        {
            base.Dispose( disposing );
        }
    }

    #endregion

    #region " view model "

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0032:Use auto property", Justification = "<Pending>" )]
    private AppSettingsEditorViewModel? _settingsEditorViewModel;

    /// <summary>   Gets or sets the application settings editor view model. </summary>
    /// <value> The application settings editor view model. </value>
    [CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public AppSettingsEditorViewModel? SettingsEditorViewModel
    {
        get => this._settingsEditorViewModel;
        set
        {
            this._settingsEditorViewModel = value;
#if NET8_0_OR_GREATER
            this.DataContext = value;
#endif
            this.SetAppSettingsScribe( value?.Scribe );
            this.BindPropertyGrid();
        }
    }

    private AppSettingsScribe? _scribe;

    /// <summary>   Gets or sets the <see cref="AppSettingsScribe">settings scribe</see>. </summary>
    /// <value> The scribe. </value>
    [CLSCompliant( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public AppSettingsScribe? Scribe
    {
        get => this._scribe;
        set => this.SetAppSettingsScribe( value );
    }

    /// <summary>   Sets the application settings source. </summary>
    /// <param name="value"> The application settings source. </param>
    private void SetAppSettingsScribe( AppSettingsScribe? value )
    {
        if ( this._scribe is not null )
            this.AvailableSettingsCombo.SelectedIndexChanged -= this.AvailableSettingsCombo_SelectedIndexChanged;

        this._scribe = value;
        if ( this._scribe is null )
        {
            this.ResetMenuItem.Enabled = false;
            this.ReadMenuItem.Enabled = false;
            this.SaveMenuItem.Enabled = false;
            this.SaveExitMenuItem.Enabled = false;
            this.AvailableSettingsCombo.Enabled = false;
        }
        else
        {
            this.SaveExitMenuItem.Enabled = this._scribe.CanWrite;
            if ( this._scribe.SettingsDictionary is not null )
            {
                this.AvailableSettingsCombo.Enabled = this._scribe.SettingsDictionary.Count > 0;
                this.AvailableSettingsCombo.ComboBox.Items.Clear();
                this.AvailableSettingsCombo.ComboBox.DataSource = this._scribe.SettingsDictionary.ToList();
                this.AvailableSettingsCombo.ComboBox.BindingContext = this.BindingContext;
                this.AvailableSettingsCombo.ComboBox.DisplayMember = nameof( DictionaryEntry.Key );
                this.AvailableSettingsCombo.ComboBox.ValueMember = nameof( DictionaryEntry.Value );
                this.AvailableSettingsCombo.SelectedIndexChanged += this.AvailableSettingsCombo_SelectedIndexChanged;
                this._scribe.SelectSettings( this._scribe.SelectedSection );
            }
        }
    }

    #endregion

    #region " property grid binding "

    /// <summary>   Bind property grid. </summary>
    /// <remarks>
    /// 2023-05-02. <para>
    /// While the property grid selected property can be properly updated by direct settings, with
    /// binding the grid needs to explicitly be refreshed for the property change to take effect.
    /// This method invokes the property grid refresh method on the property grid thread to do the
    /// update.
    /// </para>
    /// </remarks>
    private void BindPropertyGrid()
    {
        this.PropertyGrid.DataBindings.Add( new Binding( nameof( System.Windows.Forms.PropertyGrid.SelectedObject ),
            this.appSettingsEditorViewModelBindingSource, nameof( ViewModels.AppSettingsEditorViewModel.SelectedSettings ), true,
            DataSourceUpdateMode.OnPropertyChanged ) );
        this.SettingsEditorViewModel!.PropertyChanged += ( object? sender, PropertyChangedEventArgs e ) => this.PropertyGrid.Invoke( () => this.PropertyGrid.Refresh() );
    }

    #endregion

    #region " ui handlers "

    /// <summary>
    /// Event handler. Called by AvailableSettingsCombo for selected index changed events.
    /// </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void AvailableSettingsCombo_SelectedIndexChanged( object? sender, EventArgs e )
    {
        try
        {
            this._settingsEditorViewModel?.SelectSettingsCommand.Execute( this.AvailableSettingsCombo.Text );
        }
        catch ( Exception ex )
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
            _ = System.Windows.Forms.MessageBox.Show( ex.ToString(), "Exception Resetting Settings", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly );
        }
        finally
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
    }

    #endregion

    #region " ui default handlers "

    /// <summary>   Event handler. Called by CancelButton for click events. </summary>
    /// <remarks>   David, 202-09-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void CancelButton_Click( object? sender, EventArgs e )
    {
        this.Close();
    }

    /// <summary>   Event handler. Called by AcceptButton for click events. </summary>
    /// <remarks>   David, 202-09-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void AcceptButton_Click( object? sender, EventArgs e )
    {
        this.SettingsEditorViewModel?.WriteCommand.ExecuteAsync( SynchronizationContext.Current ).Wait();
        this.Close();
    }

    /// <summary>   Event handler. Called by SaveExitMenuItem for click events. </summary>
    /// <remarks>   David, 202-09-12. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Event information. </param>
    private void SaveExitMenuItem_Click( object? sender, EventArgs e )
    {
        this.SettingsEditorViewModel?.WriteCommand.ExecuteAsync( SynchronizationContext.Current ).Wait();
        this.Close();
    }

    /// <summary> Exit menu item click. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
    private void ExitMenuItem_Click( object? sender, EventArgs e )
    {
        this.Close();
    }

    #endregion
}
