
namespace cc.isr.Json.AppSettings.WinForms;

partial class JsonSettingsEditorControl
{
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing )
    {
        if (disposing)
        {
            components?.Dispose();
        }
        base.Dispose( disposing );
    }

    #region " component designer generated code "

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this.PropertyGrid = new PropertyGrid();
        this.MenuStrip = new MenuStrip();
        this.FileMenuItem = new ToolStripMenuItem();
        this.ResetMenuItem = new ToolStripMenuItem();
        this.ReadMenuItem = new ToolStripMenuItem();
        this.SaveMenuItem = new ToolStripMenuItem();
        this.AvailableSettingsCombo = new ToolStripComboBox();
        this.appSettingsEditorViewModelBindingSource = new BindingSource( this.components );
        this.MenuStrip.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize) this.appSettingsEditorViewModelBindingSource).BeginInit();
        this.SuspendLayout();
        // 
        // PropertyGrid
        // 
        this.PropertyGrid.Dock = DockStyle.Fill;
        this.PropertyGrid.Location = new Point( 0, 24 );
        this.PropertyGrid.Margin = new Padding( 4, 2, 4, 2 );
        this.PropertyGrid.Name = "PropertyGrid";
        this.PropertyGrid.Size = new Size( 517, 435 );
        this.PropertyGrid.TabIndex = 3;
        // 
        // MenuStrip
        // 
        this.MenuStrip.Items.AddRange( new ToolStripItem[] { this.FileMenuItem } );
        this.MenuStrip.Location = new Point( 0, 0 );
        this.MenuStrip.Name = "MenuStrip";
        this.MenuStrip.Padding = new Padding( 7, 2, 0, 2 );
        this.MenuStrip.Size = new Size( 517, 24 );
        this.MenuStrip.TabIndex = 4;
        this.MenuStrip.Text = "Menu Strip";
        // 
        // FileMenuItem
        // 
        this.FileMenuItem.DropDownItems.AddRange( new ToolStripItem[] { this.ResetMenuItem, this.ReadMenuItem, this.SaveMenuItem, this.AvailableSettingsCombo } );
        this.FileMenuItem.Name = "FileMenuItem";
        this.FileMenuItem.Size = new Size( 37, 20 );
        this.FileMenuItem.Text = "&File";
        // 
        // ResetMenuItem
        // 
#if NET8_0_OR_GREATER
        this.ResetMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "RestoreCommand", true ) );
#endif
        this.ResetMenuItem.Name = "ResetMenuItem";
        this.ResetMenuItem.Size = new Size( 181, 22 );
        this.ResetMenuItem.Text = "R&eset";
        this.ResetMenuItem.ToolTipText = "Restores the persisted application settings values to their corresponding default properties";
        // 
        // ReadMenuItem
        // 
#if NET8_0_OR_GREATER
        this.ReadMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "ReadCommand", true ) );
#endif
        this.ReadMenuItem.Name = "ReadMenuItem";
        this.ReadMenuItem.Size = new Size( 181, 22 );
        this.ReadMenuItem.Text = "&Read";
        this.ReadMenuItem.ToolTipText = "Reads the current settings from the application data folder";
        // 
        // SaveMenuItem
        // 
#if NET8_0_OR_GREATER
        this.SaveMenuItem.DataBindings.Add( new Binding( "Command", this.appSettingsEditorViewModelBindingSource, "WriteCommand", true ) );
#endif
        this.SaveMenuItem.Name = "SaveMenuItem";
        this.SaveMenuItem.Size = new Size( 181, 22 );
        this.SaveMenuItem.Text = "&Save";
        this.SaveMenuItem.ToolTipText = "Write the settings to the application data folder";
        // 
        // AvailableSettingsCombo
        // 
        this.AvailableSettingsCombo.Name = "AvailableSettingsCombo";
        this.AvailableSettingsCombo.Size = new Size( 121, 23 );
        this.AvailableSettingsCombo.ToolTipText = "Select a setting from the combo box";
        // 
        // appSettingsEditorViewModelBindingSource
        // 
        this.appSettingsEditorViewModelBindingSource.DataSource = typeof( ViewModels.AppSettingsEditorViewModel );
        // 
        // JsonSettingsEditorControl
        // 
        this.AutoScaleDimensions = new SizeF( 7F, 15F );
        this.AutoScaleMode = AutoScaleMode.Font;
        this.Controls.Add( this.PropertyGrid );
        this.Controls.Add( this.MenuStrip );
        this.Margin = new Padding( 4, 3, 4, 3 );
        this.Name = "JsonSettingsEditorControl";
        this.Size = new Size( 517, 459 );
        this.MenuStrip.ResumeLayout( false );
        this.MenuStrip.PerformLayout();
        ((System.ComponentModel.ISupportInitialize) this.appSettingsEditorViewModelBindingSource).EndInit();
        this.ResumeLayout( false );
        this.PerformLayout();
    }

    #endregion

    private System.Windows.Forms.PropertyGrid PropertyGrid;
    private System.Windows.Forms.MenuStrip MenuStrip;
    private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ResetMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ReadMenuItem;
    private System.Windows.Forms.ToolStripMenuItem SaveMenuItem;
    private ToolStripComboBox AvailableSettingsCombo;
    private BindingSource appSettingsEditorViewModelBindingSource;
}
