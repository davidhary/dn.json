namespace cc.isr.Json.AppSettings.Services;

/// <summary>   A window forms asynchronous helper. </summary>
/// <remarks>   2023-04-29. </remarks>
public class WinFormsAsyncHelper
{
    #region " singleton "

    private static WinFormsAsyncHelper? _instance;

    /// <summary>   Gets an instance. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="marshallingControl">   The marshalling control. </param>
    /// <returns>   The instance. </returns>
    public static WinFormsAsyncHelper GetInstance( Control marshallingControl )
    {
        return _instance ??= new WinFormsAsyncHelper( marshallingControl );
    }

    private readonly Control _marshallingControl;

    /// <summary>   Constructor. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="marshallingControl">   The marshalling control. </param>
    private WinFormsAsyncHelper( Control marshallingControl ) => this._marshallingControl = marshallingControl;

    #endregion

    /// <summary>
    /// Executes the asynchronous on a different thread, and waits for the result.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="function"> The function. </param>
    /// <returns>   A T? </returns>
    public async Task<T?> InvokeAsync<T>( Func<T> function )
    {
        TaskCompletionSource<bool> taskCompletionSource = new();
        IAsyncResult? asyncResult = this._marshallingControl.BeginInvoke( function );

        RegisteredWaitHandle registeredWaitHandle = ThreadPool.RegisterWaitForSingleObject(
               asyncResult.AsyncWaitHandle,
               new WaitOrTimerCallback( InvokeAsyncCallBack ),
               taskCompletionSource,
               millisecondsTimeOutInterval: -1,
               executeOnlyOnce: true );

        _ = await taskCompletionSource.Task;

        object? returnObject = this._marshallingControl.EndInvoke( asyncResult );
        return ( T? ) returnObject;
    }

    /// <summary>
    /// Executes the asynchronous call back on a different thread, and waits for the result.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="state">    The state. </param>
    /// <param name="timeOut">  True to time out. </param>
    private static void InvokeAsyncCallBack( object? state, bool timeOut )
    {
        try
        {
            if ( state is TaskCompletionSource<bool> source )
            {
                _ = source.TrySetResult( timeOut );
            }
        }
        catch ( Exception ex )
        {
            if ( state is TaskCompletionSource<bool> source )
            {
                _ = source.TrySetException( ex );
            }
        }
    }
}
