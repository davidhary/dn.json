using cc.isr.Json.AppSettings.ViewModels.Bases;

namespace cc.isr.Json.AppSettings.Services;

/// <summary>   A service for accessing window forms dialogs information. </summary>
/// <remarks>   2023-04-29. </remarks>
public class WinFormsDialogService : IDialogService
{
    private readonly Dictionary<Type, Type> _controllerFormTypeLookup = [];

    private static readonly SynchronizationContext? _winFormsSyncContext
        = SynchronizationContext.Current;

    private static WinFormsAsyncHelper? _asyncHelper;

    /// <summary>   Helper method that register window forms asynchronous. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <param name="asyncHelper">  The asynchronous helper. </param>
    public static void RegisterWinFormsAsyncHelper( WinFormsAsyncHelper asyncHelper )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( asyncHelper );
#else
        if ( asyncHelper is null ) throw new ArgumentNullException( nameof( asyncHelper ) );
#endif

        _asyncHelper = asyncHelper;
    }

    /// <summary>   Registers the user interface controller. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
    /// <param name="uiController"> The controller. </param>
    /// <param name="viewAsForm">   The view as form. </param>
    public void RegisterUIController( Type uiController, Type viewAsForm )
    {
        if ( _winFormsSyncContext is null )
        {
            throw new InvalidOperationException( "Fail to retrieve a WinForms synchronization context, " +
                "which is needed to run asynchronous Dialog Service calls." );
        }

        this._controllerFormTypeLookup.Add( uiController, viewAsForm );
    }

    /// <summary>   Shows the modal asynchronously. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <exception cref="Exception">    Thrown when an exception error condition occurs. </exception>
    /// <param name="registeredController"> The registered controller. </param>
    /// <returns>   The show modal. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0072:Add missing cases", Justification = "<Pending>" )]
    [CLSCompliant( false )]
    public async Task<string> ShowModalAsync( ModalViewModelBase registeredController )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( registeredController );
#else
        if ( registeredController is null ) throw new ArgumentNullException( nameof( registeredController ) );
#endif

        if ( this._controllerFormTypeLookup.TryGetValue( registeredController.GetType(), out Type? viewType ) )
        {
            if ( Activator.CreateInstance( viewType ) is Form view )
            {
#if NET8_0_OR_GREATER
                view.DataContext = registeredController;
#endif

                // Since the Buttons are supposed to be command-bound
                // the ViewModel should know the dialog result via binding.
                DialogResult dialogResult = await _asyncHelper!.InvokeAsync( view.ShowDialog );

                string returnValue = dialogResult switch
                {
                    DialogResult.OK => "OK",
                    DialogResult.Cancel => "Cancel",
                    _ => throw new InvalidOperationException( $"Unknown dialog result: {dialogResult}" ),
                };

                return returnValue;
            }

            throw new InvalidOperationException( $"Could not create the view of type {viewType}." );
        }

        throw new InvalidOperationException( $"Could not find a view for controller of type {registeredController.GetType()}." );
    }

    /// <summary>   Shows the message box asynchronously. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="title">    The title. </param>
    /// <param name="heading">  The heading. </param>
    /// <param name="message">  The message. </param>
    /// <param name="buttons">  A variable-length parameters list containing buttons. </param>
    /// <returns>   The show message box. </returns>
    public async Task<string> ShowMessageBoxAsync( string title, string heading, string message, params string[] buttons )
    {
#if NET8_0_OR_GREATER
        TaskDialogPage mainDialogPage = new()
        {
            Caption = title,
            Heading = heading,
            Text = message,

            Icon = TaskDialogIcon.Information
        };

        TaskDialogButtonCollection taskDialogButtons = [];

        foreach ( string buttonString in buttons )
        {
            // Each of the buttons will be enabled and close the MessageBox.
            taskDialogButtons.Add(
                new TaskDialogButton(
                    buttonString,
                    enabled: true,
                    allowCloseDialog: true ) );
        }

        mainDialogPage.Buttons = taskDialogButtons;

        TaskDialogButton? result;
        result = await _asyncHelper!.InvokeAsync( () => TaskDialog.ShowDialog( mainDialogPage ) );
        return $"{result}";
#else
        DialogResult r = await _asyncHelper!.InvokeAsync( () => MessageBox.Show( message, title ) );
        return $"{r}";
#endif

    }

    /// <summary>   Sets marshalling context. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="syncContext">  Context for the synchronize. </param>
    public void SetMarshallingContext( object syncContext )
    {
        throw new NotImplementedException();
    }
}
