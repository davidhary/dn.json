namespace cc.isr.Json.AppSettings.Services;

/// <summary>   A simple service provider. </summary>
/// <remarks>   2023-04-29. </remarks>
public class SimpleServiceProvider : IServiceProvider
{
    #region " singleton "

    /// <summary>   Gets the instance. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <returns>   The instance. </returns>
    public static IServiceProvider GetInstance()
    {
        return _instance.Value;
    }

    private static readonly Lazy<IServiceProvider> _instance = new( () => new SimpleServiceProvider(), true );

    #endregion

    private IDialogService? _winFormsDialogService;

    /// <summary>   Gets the service object of the specified type. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="serviceType">  An object that specifies the type of service object to get. </param>
    /// <returns>
    /// A service object of type <paramref name="serviceType" />.  
    /// 
    ///  -or-  
    /// 
    ///  <see langword="null" /> if there is no service object of type <paramref name="serviceType" />.
    ///  
    /// </returns>
    public object GetService( Type serviceType )
    {
        return serviceType switch
        {
            Type requestedType when typeof( IDialogService ).IsAssignableFrom( requestedType )
                => this._winFormsDialogService ??= new WinFormsDialogService(),

            Type requestedType when typeof( WinFormsDialogService ).IsAssignableFrom( requestedType )
                => this._winFormsDialogService ??= new WinFormsDialogService(),

            _ => throw new NotImplementedException( $"Requested service '{serviceType}' is not supported." )
        };
    }
}
