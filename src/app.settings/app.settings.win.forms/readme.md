# About

[cc.isr.Json.AppSettings.WinForms] is a .Net library providing a 
Windows form and and control for editing Json configuration settings.

# How to Use

The following example is taken from the [Editor demo] project of the [Json Repository]. 

An editor form is instantiated and opened as follows:
```
_ = Application.SetHighDpiMode( HighDpiMode.SystemAware );
Application.EnableVisualStyles();
Application.SetCompatibleTextRenderingDefault( false );

var serviceProvider = SimpleServiceProvider.GetInstance();

// use the scope to select the active application settings file.

var assemblyFileInfo = new AssemblyFileInfo( typeof( Program ).Assembly );

string scopeFile = scope == ScopeOption.AppContext
    ? assemblyFileInfo.AppContextSettingsPath!
    : scope == ScopeOption.ThisUser
        ? assemblyFileInfo.ThisUserSettingsPath!
        : assemblyFileInfo.AllUsersSettingsPath!;


// build the app settings source model for the View Model

AppSettingsScribe model = new( new object[] { new Settings(), new TestSettings() },
                                                assemblyFileInfo.AppContextSettingsPath!,
                                                scopeFile );

// build the view model with the model and service provider

AppSettingsEditorViewModel viewModel = new( model, serviceProvider );

Form form = new cc.isr.Json.AppSettings.WinForms.JsonSettingsEditorForm( "Settings editor demo", viewModel );

// run the application 

Application.Run( form );
```

An editor form using the ``JsonSettingsEditorControl` is instantiated and opened as follows:
```
_ = Application.SetHighDpiMode( HighDpiMode.SystemAware );
Application.EnableVisualStyles();
Application.SetCompatibleTextRenderingDefault( false );

var serviceProvider = SimpleServiceProvider.GetInstance();

// use the scope to select the active application settings file.

var assemblyFileInfo = new AssemblyFileInfo( typeof( Program ).Assembly );

string scopeFile = scope == ScopeOption.AppContext
    ? assemblyFileInfo.AppContextSettingsPath!
    : scope == ScopeOption.ThisUser
        ? assemblyFileInfo.ThisUserSettingsPath!
        : assemblyFileInfo.AllUsersSettingsPath!;


// build the app settings source model for the View Model

AppSettingsScribe model = new( new object[] { new Settings(), new TestSettings() },
                                                assemblyFileInfo.AppContextSettingsPath!,
                                                scopeFile );

AppSettingsEditorViewModel viewModel = new( model, serviceProvider );

var control = new isr.Json.AppSettings.WinForms.JsonSettingsEditorControl {
    AppSettingsEditorViewModel = viewModel
};

// add the control to a UI form.

Form form = new() {
    Text = "Settings editor control demo",
    ClientSize = new Size( 427, 359 )
};
form.Controls.Add( control );
control.Dock = DockStyle.Fill;

// run the application 

Application.Run( form );
```
where `editor` is an `EditorOption` `enum` .

# Key Features

* User interface for editing Json based configuration settings.

# Main Types

The main types provided by this library are:

* _JsonSettingsEditorControl_ A JSON configuration settings editor control.
* _JsonSettingsEditorForm_ An editor form for JSON configuration settings.

# Feedback

[cc.isr.Json.AppSettings.WinForms] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Json Repository].

[Json Repository]: https://bitbucket.org/davidhary/dn.json
[Editor demo]: https://bitbucket.org/davidhary/dn.json/src/main/src/app.settings/app.settings.win.forms.demo/
[cc.isr.Json.AppSettings.WinForms]: https://bitbucket.org/davidhary/dn.json/src/main/src/app.settings/app.settings.win.forms/

