namespace Serilog.Events;

/// <summary>   Specifies the meaning and relative importance of a log event. </summary>
/// <remarks>   2023-05-12. </remarks>
public enum LogEventLevel
{
    /// <summary>   An enum constant representing the verbose option.
    /// Anything and everything you might want to know about a running block of code.</summary>
    Verbose,

    /// <summary>   An enum constant representing the debug option. 
    /// Internal system events that aren't necessarily observable from the outside.</summary>
    Debug,

    /// <summary>   An enum constant representing the information option. 
    /// The lifeblood of operational intelligence - things happen.</summary>
    Information,

    /// <summary>   An enum constant representing the warning option. 
    /// Service is degraded or endangered.</summary>
    Warning,

    /// <summary>   An enum constant representing the error option. 
    /// Functionality is unavailable, invariants are broken or data is lost.</summary>
    Error,

    /// <summary>   An enum constant representing the fatal option. 
    /// If you have a pager, it goes off when one of these occurs. </summary>
    Fatal
}
