using cc.isr.Json.AppSettings.Models;

namespace cc.isr.Json.AppSettings.ViewModels.Providers;

/// <summary>   Provides settings for all tests. </summary>
/// <remarks>   2023-04-24. </remarks>
internal sealed partial class TestSiteSettings : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " default instance singleton "

    /// <summary>   Creates the instance which stores and reads back the default values. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The default instance. </returns>
    private static TestSiteSettings CreateDefaultInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( TestSiteSettings ).Assembly, null, ".Settings", ".json" );

        // must copy application context settings here to clear any bad settings files.

        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.AllUsersAssemblyFilePath! );
        AppSettingsScribe.CopySettings( ai.AppContextAssemblyFilePath!, ai.ThisUserAssemblyFilePath! );

        TestSiteSettings ti = new();

        // write the default settings to the user settings file

        AppSettingsScribe.WriteSettings( ai.AllUsersAssemblyFilePath!, nameof( TestSiteSettings ), ti );

        // read back the settings from the user settings file.

        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( TestSiteSettings ), ti );

        return ti;
    }

    /// <summary>   Gets the Default instance. </summary>
    /// <value> The default instance. </value>
    public static TestSiteSettings DefaultInstance => _defaultInstance.Value;

    private static readonly Lazy<TestSiteSettings> _defaultInstance = new( CreateDefaultInstance, true );

    #endregion
}
