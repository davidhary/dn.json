using System.Diagnostics;
using cc.isr.Json.AppSettings.Models;

namespace cc.isr.Json.AppSettings.ViewModels.Providers;

/// <summary>   Provides settings for all tests. </summary>
/// <remarks>   2023-04-24. </remarks>
internal sealed partial class TestSiteSettings : CommunityToolkit.Mvvm.ComponentModel.ObservableObject
{
    #region " test instance singleton "

    /// <summary>   Creates the instance which gets clears and then reads back the user settings file. </summary>
    /// <remarks>   2023-05-15. </remarks>
    /// <returns>   The test instance. </returns>
    private static TestSiteSettings CreateTestInstance()
    {
        // get assembly files using the .Logging suffix.

        AssemblyFileInfo ai = new( typeof( TestSiteSettings ).Assembly, null, ".Settings", ".json" );

        TestSiteSettings ti = new();

        // clear the default values:
        ti.Enabled = !ti.Enabled;
        ti.All = !ti.All;
        ti.MessageLevel = ti.MessageLevel switch
        {
            TraceLevel.Off => TraceLevel.Error,
            TraceLevel.Error => TraceLevel.Warning,
            TraceLevel.Warning => TraceLevel.Info,
            TraceLevel.Info => TraceLevel.Verbose,
            TraceLevel.Verbose => TraceLevel.Info,
            _ => TraceLevel.Error,
        };
        ti.IPv4Prefixes = "10.1|192.168";
        ti.TimeZones = "Central Standard Time|Pacific Standard Time";
        ti.TimeZoneOffsets = "-6|-8";

        // read back the settings from the user settings file.

        AppSettingsScribe.ReadSettings( ai.AllUsersAssemblyFilePath!, nameof( TestSiteSettings ), ti );

        return ti;
    }

    /// <summary>
    /// Gets the test instance, which is read after changing the default settings thus ensuring that
    /// new settings are indeed read.
    /// </summary>
    /// <value> The test instance. </value>
    public static TestSiteSettings TestInstance => _testInstance.Value;

    private static readonly Lazy<TestSiteSettings> _testInstance = new( CreateTestInstance, true );

    #endregion
}
