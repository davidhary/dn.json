# About
[cc.isr.Jason.AppSettings.ViewModels.MSTest] demonstrates the storing and restoring Json application test settings capabilities of the [cc.isr.Jason.AppSettings.ViewModels]application settings view model.

# How to Use

The `AppSettingsEditorViewModel` class provides the relay commands which accomplish the read , writing and restoring of application settings JSon files consisting of multiple Json sections. .

# Key Features

* Provides Json-based configuration settings of multiple Json sections;

# Main Types

The main types provided by this library are:

* `AppSettingsScribe` reads, writes and restores application settings objects.
* `AppSettingsEditorViewModel` a UI controller class for providing UI access to the model.
* `IDialogService` for allowing the restore function to present a user dialog for approving the restore action.

# Feedback

[cc.isr.Jason.AppSettings.ViewModels.MSTest] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Json Repository].

[Json Repository]: https://bitbucket.org/davidhary/dn.json
[cc.isr.Jason.AppSettings.ViewModels]: https://bitbucket.org/davidhary/dn.json/main/src/app.settings/app.settings.view.models
[cc.isr.Jason.AppSettings.ViewModels.MSTest]: https://bitbucket.org/davidhary/dn.json/main/src/app.settings/app.settings.view.models.MSTest
