using System.Diagnostics;
using Serilog.Events;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.ViewModels.Providers;
using cc.isr.Json.AppSettings.ViewModels.Providers.Extensions;
using static cc.isr.Json.AppSettings.ViewModels.Tests.SerilogSettings;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary>   (Unit Test Class) an application Serilog settings tests. </summary>
/// <remarks>   2023-05-11. </remarks>
[TestClass]
public class AppSerilogSettingsTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";

            // get assembly files using the .Serilog suffix.

            AssemblyFileInfo = new AssemblyFileInfo( typeof( AppSerilogSettingsTests ).Assembly, null, ".Serilog", ".json" );

            // must copy application context settings here to clear any bad settings file.

            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );
            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.ThisUserAssemblyFilePath! );

            ThisTestSiteSettings = new();
            ThisSerilogSettings = new();

            Scribe = new AppSettingsScribe( [nameof( TestSiteSettings ), SectionName],
                                            [ThisTestSiteSettings, ThisSerilogSettings],
                                            AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );

        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Console.WriteLine( $"Failed initializing the test class: {ex}" );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    private IDisposable? _loggerScope;

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( AppSettingsScribe ).Assembly.FullName}" );
        this._loggerScope = Logger?.BeginScope( this.TestContext?.TestName ?? string.Empty );
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        this._loggerScope?.Dispose();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<AppSerilogSettingsTests>? Logger { get; } = LoggerProvider.CreateLogger<AppSerilogSettingsTests>();

    public static AssemblyFileInfo? AssemblyFileInfo { get; set; }

    /// <summary>   Gets or sets the scribe. </summary>
    /// <value> The scribe. </value>
    public static AppSettingsScribe? Scribe { get; set; }

    /// <summary>   Gets the test site settings. </summary>
    /// <value> The test site settings. </value>
    private static TestSiteSettings? ThisTestSiteSettings { get; set; }

    /// <summary>   Gets the Serilog settings. </summary>
    /// <value> The Serilog settings. </value>
    private static SerilogSettings? ThisSerilogSettings { get; set; }

    private static LogEventLevel ChangeLogEventLevel( LogEventLevel fromLogEventLevel )
    {
        return fromLogEventLevel switch
        {
            LogEventLevel.Verbose => LogEventLevel.Debug,
            LogEventLevel.Debug => LogEventLevel.Information,
            LogEventLevel.Information => LogEventLevel.Warning,
            LogEventLevel.Warning => LogEventLevel.Error,
            LogEventLevel.Error => LogEventLevel.Fatal,
            LogEventLevel.Fatal => LogEventLevel.Verbose,
            _ => LogEventLevel.Fatal,
        };
    }

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) application settings file name should build. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void AppSettingsFileNameShouldBuild()
    {
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AppContextAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AllUsersAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.ThisUserAssemblyFilePath );
    }

    /// <summary>   (Unit Test Method) tests class initialization. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod]
    public void ClassInitializeTest()
    {
        Assert.IsNotNull( ThisTestSiteSettings, $"{nameof( ThisTestSiteSettings )} should initialize" );
        Assert.IsNotNull( ThisSerilogSettings!.LevelSwitches, $"{nameof( ThisSerilogSettings.LevelSwitches )} should initialize" );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.DebugSwitch,
            $"{nameof( ThisSerilogSettings.LevelSwitches )}.{nameof( SerilogSettingsLevelSwitches.DebugSwitch )} should be read correctly" );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.ConsoleSwitch,
            $"{nameof( ThisSerilogSettings.LevelSwitches )}.{nameof( SerilogSettingsLevelSwitches.ConsoleSwitch )} should be read correctly" );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.TraceSwitch,
            $"{nameof( ThisSerilogSettings.LevelSwitches )}.{nameof( SerilogSettingsLevelSwitches.TraceSwitch )} should be read correctly" );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.FileSwitch,
            $"{nameof( ThisSerilogSettings.LevelSwitches )}.{nameof( SerilogSettingsLevelSwitches.FileSwitch )} should be read correctly" );

        Assert.IsNotNull( ThisSerilogSettings?.MinimumLevel, $"{nameof( ThisSerilogSettings.MinimumLevel )} should initialize" );
        Assert.IsNotNull( ThisSerilogSettings?.MinimumLevel?.Default,
            $"{nameof( ThisSerilogSettings.MinimumLevel )}.{nameof( SerilogSettingsMinimumLevel.Default )} should initialize" );
        Assert.AreEqual( LogEventLevel.Debug, ThisSerilogSettings.MinimumLevel.Default,
            $"{nameof( ThisSerilogSettings.MinimumLevel )}.{nameof( SerilogSettingsMinimumLevel.Default )} should be read correctly" );
        Assert.IsNotNull( ThisSerilogSettings?.MinimumLevel.Override,
            $"{nameof( ThisSerilogSettings.MinimumLevel )}.{nameof( SerilogSettingsMinimumLevel.Override )} should initialize" );
        Assert.AreEqual( LogEventLevel.Information, ThisSerilogSettings.MinimumLevel.Override.Default,
            $"{nameof( ThisSerilogSettings.MinimumLevel )}.{nameof( SerilogSettingsMinimumLevel.Override )}.{nameof( SerilogSettingsMinimumLevel.Override.Default )} should be read correctly" );

        Assert.IsNotNull( ThisSerilogSettings.Providers, $"{nameof( ThisSerilogSettings.Providers )} should initialize" );
        Assert.AreEqual( 4, ThisSerilogSettings.Providers.Count, $"{nameof( ThisSerilogSettings.Providers )} should include the expected number of elements" );
        Assert.AreEqual( "Serilog.Sinks.Trace", ThisSerilogSettings.Providers[0], $"the first provider should be read" );

        Assert.IsNotNull( ThisSerilogSettings.Sinks, $"{nameof( ThisSerilogSettings.Sinks )} should initialize" );
        Assert.AreEqual( 4, ThisSerilogSettings.Sinks.Count, $"{nameof( ThisSerilogSettings.Sinks )} should include the expected number of elements" );

        Queue<string> names = new( ["Trace", "Console", "Debug", "Async"] );
        Queue<string> levelSwitches = new( ["$traceSwitch", "$consoleSwitch", "$debugSwitch", "$fileSwitch"] );
        Queue<string> outputTemplates = new(
        [ "t::{Timestamp:HH:mm:ss.fff zzz}, [{Level:u3}], ({SourceContext}), {Message}{NewLine}{Exception}",
          "c::{Timestamp:HH:mm:ss.fff zzz}, [{Level:u3}], ({SourceContext}), {Message}{NewLine}{Exception}",
          "d::{Timestamp:HH:mm:ss.fff zzz}, [{Level:u3}], ({SourceContext}), {Message}{NewLine}{Exception}",
          "{Timestamp:HH:mm:ss.fff zzz}, [{Level:u3}], ({SourceContext}), {Message}{NewLine}{Exception}",
        ] );
        foreach ( SerilogSettingsSinks destination in ThisSerilogSettings.Sinks )
        {
            Assert.IsNotNull( destination.Args, $"{nameof( SerilogSettingsSinks )}.{nameof( SerilogSettingsSinks.Args )} should initialize" );
            Assert.AreEqual( names.Dequeue(), destination.Name, $"{nameof( SerilogSettingsSinks )}.{nameof( SerilogSettingsSinks.Name )} should be read" );
            SerilogSettingsArgs? args = destination.Args;
            if ( string.Equals( destination.Name, "Async", StringComparison.Ordinal ) )
            {
                Assert.IsNotNull( args.Configure, $"{nameof( ThisSerilogSettings.Sinks )}.{nameof( SerilogSettingsSinks.Args.Configure )} should initialize" );
                Assert.AreEqual( 1, args.Configure.Count, $"{nameof( ThisSerilogSettings.Sinks )}.{nameof( SerilogSettingsSinks.Args.Configure )} should include the expected number of elements" );
                Assert.AreEqual( "Async", destination.Name, $"{nameof( SerilogSettingsSinks )}.{nameof( SerilogSettingsSinks.Args )}.{nameof( SerilogSettingsArgs.Configure )}.{nameof( SerilogSettingsSinks.Name )} should be read" );
                args = args.Configure![0].Args;
                Assert.AreEqual( "cc.isr.Logging.Orlog.SerilogHooks::MyHeaderWriter, cc.isr.Logging.Orlog", args!.Hooks, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.Hooks )} should be read" );
                Assert.AreEqual( "../_logs/%ComputerName%_.log", args.Path, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.Path )} should be read" );
                Assert.AreEqual( "Day", args.RollingInterval, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.RollingInterval )} should be read" );
                Assert.AreEqual( 7, args.RetainedFileCountLimit, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.RetainedFileCountLimit )} should be read" );
            }
            Assert.AreEqual( levelSwitches.Dequeue(), args.LevelSwitch, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.LevelSwitch )} should be read" );
            Assert.AreEqual( outputTemplates.Dequeue(), args.OutputTemplate, $"{nameof( SerilogSettingsArgs )}.{nameof( SerilogSettingsArgs.OutputTemplate )} should be read" );
        }

        Assert.IsNotNull( ThisSerilogSettings.Enrich, $"{nameof( ThisSerilogSettings.Enrich )} should initialize" );
        Assert.AreEqual( 3, ThisSerilogSettings.Enrich.Count, $"{nameof( ThisSerilogSettings.Enrich )} should include the expected number of elements" );
        Assert.AreEqual( "FromLogContext", ThisSerilogSettings.Enrich[0], $"the first enrich item should be read" );

        Assert.IsNotNull( ThisSerilogSettings.Destructures, $"{nameof( ThisSerilogSettings.Destructures )} should initialize" );
        Assert.AreEqual( 3, ThisSerilogSettings.Destructures.Count, $"{nameof( ThisSerilogSettings.Destructures )} should include the expected number of elements" );
        Assert.AreEqual( "ToMaximumDepth", ThisSerilogSettings.Destructures[0].Name, $"the first Destructure item name should be read" );
        Assert.IsNotNull( ThisSerilogSettings.Destructures[0].Args, $"{nameof( ThisSerilogSettings.Destructures )}.Args should initialize" );

        Assert.IsNotNull( ThisSerilogSettings.PropertiesElement, $"{nameof( ThisSerilogSettings.PropertiesElement )} should initialize" );
        Assert.AreEqual( "cc.isr.Json.AppSettings.ViewModels.MSTest", ThisSerilogSettings.PropertiesElement.Application, $"{nameof( ThisSerilogSettings.PropertiesElement )}.Application should be read correctly" );

        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );
        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogEventLevel.Information} {nameof( LogEventLevel )}" );

    }

    #endregion

    #region " read, write, restore tests "

    /// <summary>   (Unit Test Method) application context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void AppContextSettingsShouldRead()
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;
        string userSettingsPath = Scribe!.AppContextSettingsPath!;

        // Set the using file so that we are using only the app context files. This could write but not restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath}" );
        Assert.IsFalse( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should not be restorable when set with only {appContextSettingsPath}" );

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogEventLevel? expectedLogEventLevel = ThisSerilogSettings!.MinimumLevel!.Default ?? LogEventLevel.Fatal;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisSerilogSettings.MinimumLevel.Default = ChangeLogEventLevel( ThisSerilogSettings.MinimumLevel.Default ?? LogEventLevel.Fatal );

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogEventLevel, ThisSerilogSettings.MinimumLevel.Default );
    }

    /// <summary>   (Unit Test Method) user context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "AllUsers" )]
    [DataRow( "ThisUser" )]
    public void UserContextSettingsShouldRead( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogEventLevel? expectedLogEventLevel = ThisSerilogSettings?.MinimumLevel?.Default ?? LogEventLevel.Fatal;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisSerilogSettings!.MinimumLevel!.Default = ChangeLogEventLevel( ThisSerilogSettings.MinimumLevel.Default ?? LogEventLevel.Fatal );

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogEventLevel, ThisSerilogSettings.MinimumLevel.Default );
    }

    /// <summary>   (Unit Test Method) user context settings should restore. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestore( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogEventLevel? expectedLogEventLevel = ThisSerilogSettings!.MinimumLevel!.Default ?? LogEventLevel.Fatal;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisSerilogSettings.MinimumLevel.Default = ChangeLogEventLevel( ThisSerilogSettings.MinimumLevel.Default ?? LogEventLevel.Fatal );

        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        LogEventLevel? expectedNewLogEventLevel = ThisSerilogSettings.MinimumLevel.Default;

        // write the new settings

        Scribe.WriteSettings();

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewLogEventLevel, ThisSerilogSettings.MinimumLevel.Default );

        // now try to restore.

        Scribe.RestoreSettings();

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogEventLevel, ThisSerilogSettings.MinimumLevel.Default );
    }

    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestoreTestSiteSettings( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        TraceLevel expectedMessageLevel = ThisTestSiteSettings!.MessageLevel;

        // alter the settings

        ThisTestSiteSettings.IPv4Prefixes += "0";

        ThisTestSiteSettings!.MessageLevel = expectedMessageLevel switch
        {
            TraceLevel.Off => TraceLevel.Error,
            TraceLevel.Error => TraceLevel.Warning,
            TraceLevel.Warning => TraceLevel.Info,
            TraceLevel.Info => TraceLevel.Verbose,
            TraceLevel.Verbose => TraceLevel.Info,
            _ => TraceLevel.Error,
        };
        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        TraceLevel expectedNewMessageLevelValue = ThisTestSiteSettings.MessageLevel;

        // write the new settings

        Scribe.WriteSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // read the saved settings

        Scribe.ReadSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewMessageLevelValue, ThisTestSiteSettings.MessageLevel );

        // now try to restore.

        Scribe.RestoreSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedMessageLevel, ThisTestSiteSettings.MessageLevel );
    }

    /// <summary>
    /// (Unit Test Method) user context settings should restore Serilog settings.
    /// </summary>
    /// <remarks>   2023-05-11. </remarks>
    /// <param name="userContext">  Context for the user. </param>
    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestoreSerilogSettings( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        LogEventLevel? expectedEventLogDefaultValue = ThisSerilogSettings!.MinimumLevel!.Default!;
        LogEventLevel? expectedEventSourceDefaultValue = ThisSerilogSettings!.MinimumLevel!.Override!.Default;

        // alter the settings

        ThisSerilogSettings.MinimumLevel.Default = ChangeLogEventLevel( ThisSerilogSettings.MinimumLevel.Default ?? LogEventLevel.Fatal );
        ThisSerilogSettings!.MinimumLevel!.Override!.Default = ChangeLogEventLevel( ThisSerilogSettings.MinimumLevel.Override.Default ?? LogEventLevel.Fatal );

        LogEventLevel? expectedNewEventLogDefaultValue = ThisSerilogSettings.MinimumLevel.Default;
        LogEventLevel? expectedNewEventSourceDefaultValue = ThisSerilogSettings.MinimumLevel.Override.Default;

        // write the new settings

        Scribe.WriteSettings( SectionName, ThisSerilogSettings! );

        // read the saved settings

        Scribe.ReadSettings( SectionName, ThisSerilogSettings! );

        // validate

        Assert.AreEqual( expectedNewEventLogDefaultValue, ThisSerilogSettings.MinimumLevel.Default );
        Assert.AreEqual( expectedNewEventSourceDefaultValue, ThisSerilogSettings.MinimumLevel.Override.Default );

        // now try to restore.

        Scribe.RestoreSettings( SectionName, ThisSerilogSettings! );

        // validate

        Assert.AreEqual( expectedEventLogDefaultValue, ThisSerilogSettings.MinimumLevel.Default );
        Assert.AreEqual( expectedEventSourceDefaultValue, ThisSerilogSettings.MinimumLevel.Override.Default );
    }

    #endregion

    #region " values tests "

    [TestMethod()]
    public void LogEventLevelsForAllProvidersShouldEqual()
    {
        Assert.IsNotNull( ThisSerilogSettings, nameof( ThisSerilogSettings.LevelSwitches ) );
        Assert.IsNotNull( ThisSerilogSettings.LevelSwitches, nameof( SerilogSettings.LevelSwitches ) );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.ConsoleSwitch );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.DebugSwitch );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.TraceSwitch );
        Assert.AreEqual( LogEventLevel.Verbose, ThisSerilogSettings.LevelSwitches.FileSwitch );
    }

    #endregion
}
