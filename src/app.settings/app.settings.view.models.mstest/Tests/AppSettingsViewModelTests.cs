using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.Models;
using System.Diagnostics;
using cc.isr.Json.AppSettings.ViewModels.Tests.Services;
using cc.isr.Json.AppSettings.ViewModels.Providers;
using cc.isr.Json.AppSettings.ViewModels.Providers.Extensions;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary>
/// (Unit Test Class) an application settings tests using the scribe relay commands for reading
/// and writing application settings.
/// </summary>
/// <remarks>   2023-05-11. </remarks>
[TestClass]
public class AppSettingsViewModelTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";

            AssemblyFileInfo = new AssemblyFileInfo( typeof( AppSettingsViewModelTests ).Assembly );

            // must copy application context settings here to clear any bad settings files.

            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );
            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.ThisUserAssemblyFilePath! );

            ThisTestSiteSettings = new();
            ThisTestSettings = new();

            Scribe = new AppSettingsScribe( [nameof( TestSiteSettings ), nameof( TestSettings )],
                                            [ThisTestSettings, ThisTestSettings],
                                            AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );
        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Console.WriteLine( $"Failed initializing the test class: {ex}" );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    private IDisposable? _loggerScope;

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( AppSettingsScribe ).Assembly.FullName}" );
        this._loggerScope = Logger?.BeginScope( this.TestContext?.TestName ?? string.Empty );
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        this._loggerScope?.Dispose();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<AppSettingsViewModelTests>? Logger { get; } = LoggerProvider.CreateLogger<AppSettingsViewModelTests>();

    public static AssemblyFileInfo? AssemblyFileInfo { get; set; }

    /// <summary>   Gets or sets the scribe. </summary>
    /// <value> The scribe. </value>
    public static AppSettingsScribe? Scribe { get; set; }

    /// <summary>   Gets the test site settings. </summary>
    /// <value> The test site settings. </value>
    private static TestSiteSettings? ThisTestSiteSettings { get; set; }

    /// <summary>   Gets the test settings. </summary>
    /// <value> The test settings. </value>
    private static TestSettings? ThisTestSettings { get; set; }

    #endregion

    #region " services "

    private string _messageBoxDialogResult = AppSettingsEditorViewModel.BUTTON_TEXT_NO;

    /// <summary>
    /// Event handler. Called by DialogService for show message box requested events.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Show message box result event information. </param>
    private void DialogService_ShowMessageBoxRequested( object? sender, ShowMessageBoxResultEventArgs e )
    {
        e.ResultButtonText = this._messageBoxDialogResult;
    }

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) tests 00 class initialize. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod]
    public void ClassInitializeTest()
    {
        Assert.IsNotNull( ThisTestSiteSettings, $"{nameof( ThisTestSiteSettings )} should initialize" );

        Assert.IsNotNull( ThisTestSettings, $"{nameof( ThisTestSettings )} should initialize" );

        Assert.AreEqual( TestSettings.EXPECTED_MESSAGE_LEVEL, ThisTestSettings.MessageLevel,
            $"{nameof( TestSettings.MessageLevel )} should be read correctly" );

        Assert.AreEqual( TestSettings.STRING_VALUE_EXPECTED, ThisTestSettings.StringValue,
            $"{nameof( ThisTestSettings.StringValue )} should be read correctly" );

        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );

        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );

    }

    /// <summary>   (Unit Test Method) application settings file name should build. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void FileNameShouldBuildTest()
    {
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AppContextAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AllUsersAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.ThisUserAssemblyFilePath );
    }

    #endregion

    #region " read, write, restore tests "

    /// <summary>   (Unit Test Method) application context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void AppContextSettingsShouldRead()
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;
        string userSettingsPath = Scribe!.AppContextSettingsPath!;

        // construct the model using only the app context files. This could write but not restore.

        AppSettingsScribe scribe = new( [ThisTestSiteSettings!, ThisTestSettings!], appContextSettingsPath, userSettingsPath );

        // validate file names

        Assert.AreEqual( scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath}" );
        Assert.IsTrue( scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath}" );
        Assert.IsFalse( scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should not be restorable when set with only {appContextSettingsPath}" );

        AppSettingsEditorViewModel viewModel = new( scribe, SimpleTestServiceProvider.GetInstance() );

        object? commandParameter = null;
        Assert.IsTrue( viewModel.ReadCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.ReadCommand )} should be executable when set with {appContextSettingsPath}" );
        Assert.IsTrue( viewModel.WriteCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.WriteCommand )} should be executable when set with {appContextSettingsPath}" );
        Assert.IsFalse( viewModel.RestoreCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.RestoreCommand )} should not be executable when set with only {appContextSettingsPath}" );

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        string expectedStringValue = ThisTestSettings!.StringValue;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisTestSettings!.StringValue += "x";

        // read all settings

        viewModel.ReadCommand.ExecuteAsync( commandParameter ).Wait();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedStringValue, ThisTestSettings.StringValue );
    }

    /// <summary>   (Unit Test Method) user context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "AllUsers" )]
    [DataRow( "ThisUser" )]
    public void UserContextSettingsShouldRead( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // construct the model using the app context and user files. This can restore.

        AppSettingsScribe scribe = new( [ThisTestSiteSettings!, ThisTestSettings!], appContextSettingsPath, userSettingsPath );

        // validate file names

        Assert.AreEqual( scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        AppSettingsEditorViewModel viewModel = new( scribe, SimpleTestServiceProvider.GetInstance() );

        object? commandParameter = null;
        Assert.IsTrue( viewModel.ReadCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.ReadCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( viewModel.WriteCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.WriteCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( viewModel.RestoreCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.RestoreCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        string expectedStringValue = ThisTestSettings!.StringValue;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += "0";
        ThisTestSettings!.StringValue += "x";

        // read all settings

        viewModel.ReadCommand.ExecuteAsync( commandParameter ).Wait();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedStringValue, ThisTestSettings.StringValue );
    }

    /// <summary>   (Unit Test Method) user context settings should restore. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public async Task UserContextSettingsShouldRestore( string userContext )
    {
        IServiceProvider service = SimpleTestServiceProvider.GetInstance();
        SimpleUnitTestDialogService? dialogService = ( SimpleUnitTestDialogService? ) service.GetService( typeof( IDialogService ) );

        Assert.IsNotNull( dialogService, $"{nameof( SimpleTestServiceProvider )} should be instantiated." );

        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // construct the model using the app context and user files. This can restore.

        AppSettingsScribe scribe = new( [ThisTestSiteSettings!, ThisTestSettings!], appContextSettingsPath, userSettingsPath );

        // validate file names

        Assert.AreEqual( scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        AppSettingsEditorViewModel viewModel = new( scribe, service );

        object? commandParameter = null;
        Assert.IsTrue( viewModel.ReadCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.ReadCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( viewModel.WriteCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.WriteCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( viewModel.RestoreCommand.CanExecute( commandParameter ), $"The {nameof( AppSettingsEditorViewModel )}.{nameof( AppSettingsEditorViewModel.RestoreCommand )} should be executable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        string expectedStringValue = ThisTestSettings!.StringValue;

        // alter the settings

        ThisTestSiteSettings.IPv4Prefixes += "0";
        ThisTestSettings.StringValue += "x";
        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        string expectedNewStringValue = ThisTestSettings.StringValue;

        // write the new settings

        viewModel.WriteCommand.ExecuteAsync( commandParameter ).Wait();

        // read all settings

        viewModel.ReadCommand.ExecuteAsync( commandParameter ).Wait();

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewStringValue, ThisTestSettings.StringValue );

        // Simulate that the user is requesting to 'New' the document,
        // but return "No" on the MessageDialogBox to actually clear it.

        dialogService!.ShowMessageBoxRequested += this.DialogService_ShowMessageBoxRequested;

        this._messageBoxDialogResult = AppSettingsEditorViewModel.BUTTON_TEXT_NO;

        // Test the first time; the state machine returns "No" the first time.

        string reply = await viewModel.UserRestoreQueryAsync();

        Assert.AreEqual( this._messageBoxDialogResult, reply );

        this._messageBoxDialogResult = AppSettingsEditorViewModel.BUTTON_TEXT_YES;

        // Test the second time; our state machine returns "Yes" the second time.

        reply = await viewModel.UserRestoreQueryAsync();
        Assert.AreEqual( this._messageBoxDialogResult, reply );

        // now try to restore.

        viewModel.RestoreCommand.ExecuteAsync( commandParameter ).Wait();

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedStringValue, ThisTestSettings.StringValue );
    }

    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestoreTestSiteSettings( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // construct the model using the app context and user files. This can restore.

        AppSettingsScribe scribe = new( [ThisTestSiteSettings!, ThisTestSettings!], appContextSettingsPath, userSettingsPath );

        // validate file names

        Assert.AreEqual( scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        TraceLevel expectedMessageLevelValue = ThisTestSiteSettings!.MessageLevel;

        // alter the settings

        ThisTestSiteSettings.IPv4Prefixes += "0";

        ThisTestSiteSettings!.MessageLevel = expectedMessageLevelValue switch
        {
            TraceLevel.Off => TraceLevel.Error,
            TraceLevel.Error => TraceLevel.Warning,
            TraceLevel.Warning => TraceLevel.Info,
            TraceLevel.Info => TraceLevel.Verbose,
            TraceLevel.Verbose => TraceLevel.Info,
            _ => TraceLevel.Error,
        };
        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        TraceLevel expectedNewMessageLevelValue = ThisTestSiteSettings.MessageLevel;

        // write the new settings

        Scribe.WriteSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // read the saved settings

        Scribe.ReadSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewMessageLevelValue, ThisTestSiteSettings.MessageLevel );

        // now try to restore.

        Scribe.RestoreSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedMessageLevelValue, ThisTestSiteSettings.MessageLevel );
    }

    #endregion

    #region " values tests "

    /// <summary>   (Unit Test Method) Boolean application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void BooleanApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.BOOLEAN_VALUE_EXPECTED, ThisTestSettings!.BooleanValue );
    }

    /// <summary>   (Unit Test Method) Boolean application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void BooleanApplicationSettingShouldChange()
    {
        bool boolValue = ThisTestSettings!.BooleanValue;
        ThisTestSettings.BooleanValue = TestSettings.BOOLEAN_VALUE_OTHER;
        Assert.AreEqual( TestSettings.BOOLEAN_VALUE_OTHER, ThisTestSettings.BooleanValue );
        ThisTestSettings.BooleanValue = boolValue;
    }

    /// <summary>   (Unit Test Method) String application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void StringApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.STRING_VALUE_EXPECTED, ThisTestSettings!.StringValue );
    }

    /// <summary>   (Unit Test Method) String application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void StringApplicationSettingShouldChange()
    {
        string boolValue = ThisTestSettings!.StringValue;
        ThisTestSettings.StringValue = TestSettings.STRIONG_VALUE_OTHER;
        Assert.AreEqual( TestSettings.STRIONG_VALUE_OTHER, ThisTestSettings.StringValue );
        ThisTestSettings.StringValue = boolValue;
    }

    /// <summary>   (Unit Test Method) Integer application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void IntegerApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.INTEGER_VALUE_EXPECTED, ThisTestSettings!.IntegerValue );
    }

    /// <summary>   (Unit Test Method) Integer application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void IntegerApplicationSettingShouldChange()
    {
        int boolValue = ThisTestSettings!.IntegerValue;
        ThisTestSettings.IntegerValue = TestSettings.INTEGER_VALUE_OTHER;
        Assert.AreEqual( TestSettings.INTEGER_VALUE_OTHER, ThisTestSettings.IntegerValue );
        ThisTestSettings.IntegerValue = boolValue;
    }

    /// <summary>   (Unit Test Method) Byte application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void ByteApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.BYTE_VALUE_EXPECTED, ThisTestSettings!.ByteValue );
    }

    /// <summary>   (Unit Test Method) Byte application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void ByteApplicationSettingShouldChange()
    {
        byte boolValue = ThisTestSettings!.ByteValue;
        ThisTestSettings.ByteValue = TestSettings.BYTE_VALUE_OTHER;
        Assert.AreEqual( TestSettings.BYTE_VALUE_OTHER, ThisTestSettings.ByteValue );
        ThisTestSettings.ByteValue = boolValue;
    }

    /// <summary>   (Unit Test Method) Timespan application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void TimespanApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.TimespanValueExpected(), ThisTestSettings!.TimespanValue );
    }

    /// <summary>   (Unit Test Method) Timespan application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void TimespanApplicationSettingShouldChange()
    {
        TimeSpan boolValue = ThisTestSettings!.TimespanValue;
        ThisTestSettings.TimespanValue = TestSettings.TimespanValueOther();
        Assert.AreEqual( TestSettings.TimespanValueOther(), ThisTestSettings.TimespanValue );
        ThisTestSettings.TimespanValue = boolValue;
    }

    /// <summary>   (Unit Test Method) DateTime application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void DateTimeApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.DateTimeValueExpected(), ThisTestSettings!.DateTimeValue );
    }

    /// <summary>   (Unit Test Method) DateTime application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void DateTimeApplicationSettingShouldChange()
    {
        DateTime boolValue = ThisTestSettings!.DateTimeValue;
        ThisTestSettings.DateTimeValue = TestSettings.DateTimeValueOther();
        Assert.AreEqual( TestSettings.DateTimeValueOther(), ThisTestSettings.DateTimeValue );
        ThisTestSettings.DateTimeValue = boolValue;
    }

    /// <summary>   (Unit Test Method) TraceEventType application setting should equal. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void TraceEventTypeApplicationSettingShouldEqual()
    {
        Assert.AreEqual( TestSettings.TRACE_EVENT_TYPE_VALUE_EXPECTED, ThisTestSettings!.TraceEventTypeValue );
    }

    /// <summary>   (Unit Test Method) TraceEventType application setting should change. </summary>
    /// <remarks>   David, 2021-01-29. </remarks>
    [TestMethod()]
    public void TraceEventTypeApplicationSettingShouldChange()
    {
        TraceEventType boolValue = ThisTestSettings!.TraceEventTypeValue;
        ThisTestSettings.TraceEventTypeValue = TestSettings.TRACE_EVENT_TYPE_VALUE_OTHER;
        Assert.AreEqual( TestSettings.TRACE_EVENT_TYPE_VALUE_OTHER, ThisTestSettings.TraceEventTypeValue );
        ThisTestSettings.TraceEventTypeValue = boolValue;
    }

    #endregion
}
