using System.Diagnostics;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.ViewModels.Providers;
using cc.isr.Json.AppSettings.ViewModels.Providers.Extensions;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary>   (Unit Test Class) an application logging settings tests. </summary>
/// <remarks>   2023-05-11. </remarks>
[TestClass]
public class AppLoggingSettingsTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";

            // get assembly files using the .Logging suffix.

            AssemblyFileInfo = new AssemblyFileInfo( typeof( AppLoggingSettingsTests ).Assembly, null, ".Logging", ".json" );

            // must copy application context settings here to clear any bad settings files.

            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );
            AppSettingsScribe.CopySettings( AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.ThisUserAssemblyFilePath! );

            ThisTestSiteSettings = new();
            ThisLoggingSettings = new();

            Scribe = new AppSettingsScribe( [nameof( TestSiteSettings ), LoggingSettings.SectionName],
                                            [ThisTestSiteSettings, ThisLoggingSettings],
                                            AssemblyFileInfo.AppContextAssemblyFilePath!, AssemblyFileInfo.AllUsersAssemblyFilePath! );
        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Console.WriteLine( $"Failed initializing the test class: {ex}" );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
    }

    private IDisposable? _loggerScope;

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( AppSettingsEditorViewModel ).Assembly.FullName}" );
        this._loggerScope = Logger?.BeginScope( this.TestContext?.TestName ?? string.Empty );

    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        this._loggerScope?.Dispose();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<AppLoggingSettingsTests>? Logger { get; } = LoggerProvider.CreateLogger<AppLoggingSettingsTests>();

    public static AssemblyFileInfo? AssemblyFileInfo { get; set; }

    /// <summary>   Gets or sets the scribe. </summary>
    /// <value> The scribe. </value>
    public static AppSettingsScribe? Scribe { get; set; }

    /// <summary>   Gets the test site settings. </summary>
    /// <value> The test site settings. </value>
    private static TestSiteSettings? ThisTestSiteSettings { get; set; }

    /// <summary>   Gets the logging settings. </summary>
    /// <value> The logging settings. </value>
    private static LoggingSettings? ThisLoggingSettings { get; set; }

    private static LogLevel ChangeLogLevel( LogLevel fromLogLevel )
    {
        return fromLogLevel switch
        {
            LogLevel.Trace => LogLevel.Debug,
            LogLevel.Debug => LogLevel.Information,
            LogLevel.Information => LogLevel.Warning,
            LogLevel.Warning => LogLevel.Error,
            LogLevel.Error => LogLevel.Critical,
            LogLevel.Critical => LogLevel.Trace,
            LogLevel.None => LogLevel.Debug,
            _ => LogLevel.Debug,
        };
    }

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) tests 00 class initialize. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod]
    public void ClassInitializeTest()
    {
        Assert.IsNotNull( ThisTestSiteSettings, $"{nameof( ThisTestSiteSettings )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings, $"{nameof( ThisLoggingSettings )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings.AllProvidersLogLevels, $"{nameof( ThisLoggingSettings.AllProvidersLogLevels )} should initialize" );
        Assert.AreEqual( LogLevel.Error, ThisLoggingSettings.AllProvidersLogLevels.Default,
            $"{nameof( ThisLoggingSettings.AllProvidersLogLevels )}.{nameof( LoggingCategoryLogLevel.Default )} should be read correctly" );

        Assert.IsNotNull( ThisLoggingSettings.ConsoleLogLevel, $"{nameof( ThisLoggingSettings.ConsoleLogLevel )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings.ConsoleLogLevel.CategoryLogLevel,
            $"{nameof( ThisLoggingSettings.ConsoleLogLevel )}.{nameof( LoggingCategoryLogLevel )} should initialize" );
        Assert.AreEqual( LogLevel.Trace, ThisLoggingSettings.ConsoleLogLevel.CategoryLogLevel.CCIsr,
            $"{nameof( ThisLoggingSettings.ConsoleLogLevel )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.CCIsr )} should be read correctly" );
        Assert.IsNull( ThisLoggingSettings.ConsoleLogLevel.CategoryLogLevel.Microsoft,
            $"{nameof( ThisLoggingSettings.ConsoleLogLevel )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Microsoft )} should be null" );

        Assert.IsNotNull( ThisLoggingSettings.DebugLogLevels, $"{nameof( ThisLoggingSettings.DebugLogLevels )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings.DebugLogLevels.CategoryLogLevel,
            $"{nameof( ThisLoggingSettings.DebugLogLevels )}.{nameof( LoggingCategoryLogLevel )} should initialize" );
        Assert.AreEqual( LogLevel.Trace, ThisLoggingSettings.DebugLogLevels.CategoryLogLevel.MicrosoftExtensionsHosting,
            $"{nameof( ThisLoggingSettings.DebugLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.CCIsr )} should be read correctly" );
        Assert.IsNull( ThisLoggingSettings.DebugLogLevels.CategoryLogLevel.Microsoft,
            $"{nameof( ThisLoggingSettings.DebugLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Microsoft )} should be null" );

        Assert.IsNotNull( ThisLoggingSettings.EventSourceLogLevels, $"{nameof( ThisLoggingSettings.EventSourceLogLevels )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings.EventSourceLogLevels.CategoryLogLevel,
            $"{nameof( ThisLoggingSettings.EventSourceLogLevels )}.{nameof( LoggingCategoryLogLevel )} should initialize" );
        Assert.AreEqual( LogLevel.Information, ThisLoggingSettings.EventSourceLogLevels.CategoryLogLevel.Default,
            $"{nameof( ThisLoggingSettings.EventSourceLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Default )} should be read correctly" );
        Assert.IsNull( ThisLoggingSettings.EventSourceLogLevels.CategoryLogLevel.Microsoft,
            $"{nameof( ThisLoggingSettings.EventSourceLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Microsoft )} should be null" );

        Assert.IsNotNull( ThisLoggingSettings.EventLogLogLevels, $"{nameof( ThisLoggingSettings.EventLogLogLevels )} should initialize" );
        Assert.IsNotNull( ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel,
            $"{nameof( ThisLoggingSettings.EventLogLogLevels )}.{nameof( LoggingCategoryLogLevel )} should initialize" );
        Assert.AreEqual( LogLevel.Information, ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Default,
            $"{nameof( ThisLoggingSettings.EventLogLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Default )} should be read correctly" );
        Assert.IsNull( ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Microsoft,
            $"{nameof( ThisLoggingSettings.EventLogLogLevels )}.{nameof( LoggingCategoryLogLevel )}.{nameof( LoggingCategoryLogLevel.Microsoft )} should be null" );

        Assert.AreEqual( TraceLevel.Verbose, ThisTestSiteSettings.MessageLevel,
            $"{nameof( TestSiteSettings.MessageLevel )} should be read correctly" );

        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );
        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );

    }

    /// <summary>   (Unit Test Method) application settings file name should build. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void FileNameShouldBuildTest()
    {
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AppContextAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.AllUsersAssemblyFilePath );
        Assert.IsInstanceOfType<string>( AssemblyFileInfo?.ThisUserAssemblyFilePath );
    }

    #endregion

    #region " read, write, restore tests "

    /// <summary>   (Unit Test Method) application context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    public void AppContextSettingsShouldRead()
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;
        string userSettingsPath = Scribe!.AppContextSettingsPath!;

        // Set the using file so that we are using only the app context files. This could write but not restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath}" );
        Assert.IsFalse( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should not be restorable when set with only {appContextSettingsPath}" );

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogLevel? expectedLogLevel = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default = ChangeLogLevel( ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None );

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogLevel, ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Default );
    }

    /// <summary>   (Unit Test Method) user context settings should read. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "AllUsers" )]
    [DataRow( "ThisUser" )]
    public void UserContextSettingsShouldRead( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedTestSiteIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogLevel? expectedLogLevel = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default = ChangeLogLevel( ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None );

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedTestSiteIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogLevel, ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Default );
    }

    /// <summary>   (Unit Test Method) user context settings should restore. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestore( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        LogLevel? expectedLogLevel = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None;

        // alter the settings

        ThisTestSiteSettings!.IPv4Prefixes += 0;
        ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default = ChangeLogLevel( ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None );

        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        LogLevel? expectedNewLogLevel = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default;

        // write the new settings

        Scribe.WriteSettings();

        // read all settings

        Scribe.ReadSettings();

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewLogLevel, ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Default );

        // now try to restore.

        Scribe.RestoreSettings();

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedLogLevel, ThisLoggingSettings.EventLogLogLevels.CategoryLogLevel.Default );
    }

    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestoreTestSiteSettings( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        string expectedIPV4Prefixes = ThisTestSiteSettings!.IPv4Prefixes;
        TraceLevel expectedMessageLevel = ThisTestSiteSettings!.MessageLevel;

        // alter the settings

        ThisTestSiteSettings.IPv4Prefixes += "0";

        ThisTestSiteSettings!.MessageLevel = expectedMessageLevel switch
        {
            TraceLevel.Off => TraceLevel.Error,
            TraceLevel.Error => TraceLevel.Warning,
            TraceLevel.Warning => TraceLevel.Info,
            TraceLevel.Info => TraceLevel.Verbose,
            TraceLevel.Verbose => TraceLevel.Info,
            _ => TraceLevel.Error,
        };
        string expectedNewIPV4Prefixes = ThisTestSiteSettings.IPv4Prefixes;
        TraceLevel expectedNewMessageLevelValue = ThisTestSiteSettings.MessageLevel;

        // write the new settings

        Scribe.WriteSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // read the saved settings

        Scribe.ReadSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedNewIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedNewMessageLevelValue, ThisTestSiteSettings.MessageLevel );

        // now try to restore.

        Scribe.RestoreSettings( ThisTestSiteSettings.GetType().Name, ThisTestSiteSettings );

        // validate

        Assert.AreEqual( expectedIPV4Prefixes, ThisTestSiteSettings.IPv4Prefixes );
        Assert.AreEqual( expectedMessageLevel, ThisTestSiteSettings.MessageLevel );
    }

    /// <summary>
    /// (Unit Test Method) user context settings should restore logging settings.
    /// </summary>
    /// <remarks>   2023-05-11. </remarks>
    /// <param name="userContext">  Context for the user. </param>
    [TestMethod()]
    [DataRow( "ThisUser" )]
    [DataRow( "AllUsers" )]
    public void UserContextSettingsShouldRestoreLoggingSettings( string userContext )
    {
        string appContextSettingsPath = Scribe!.AppContextSettingsPath!;

        string userSettingsPath = string.Equals( userContext, "ThisUser", StringComparison.OrdinalIgnoreCase )
            ? AssemblyFileInfo!.ThisUserAssemblyFilePath!
            : AssemblyFileInfo!.AllUsersAssemblyFilePath!;

        // Set the user file for using the app context and user files. This can restore.

        Scribe.SetUserSettingsPath( userSettingsPath );

        // validate file names

        Assert.AreEqual( Scribe.AppContextSettingsPath, appContextSettingsPath );
        Assert.AreEqual( Scribe.UserSettingsPath, userSettingsPath );

        Assert.IsTrue( Scribe.CanRead, $"The {nameof( AppSettingsScribe )} should be readable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanWrite, $"The {nameof( AppSettingsScribe )} should be writable when set with {appContextSettingsPath} and {userSettingsPath}" );
        Assert.IsTrue( Scribe.CanRestore, $"The {nameof( AppSettingsScribe )} should be restorable when set with {appContextSettingsPath} and {userSettingsPath}" );

        // must restore all settings first

        Scribe.RestoreSettings();

        // save values for validation

        LogLevel? expectedEventLogDefaultValue = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default;
        LogLevel? expectedEventSourceDefaultValue = ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default;

        // alter the settings

        ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default = ChangeLogLevel( ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None );
        ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default = ChangeLogLevel( ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default ?? LogLevel.None );

        LogLevel? expectedNewEventLogDefaultValue = ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default;
        LogLevel? expectedNewEventSourceDefaultValue = ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default;

        // write the new settings

        Scribe.WriteSettings( LoggingSettings.SectionName, ThisLoggingSettings! );

        // read the saved settings

        Scribe.ReadSettings( LoggingSettings.SectionName, ThisLoggingSettings! );

        // validate

        Assert.AreEqual( expectedNewEventLogDefaultValue, ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default );
        Assert.AreEqual( expectedNewEventSourceDefaultValue, ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default );

        // now try to restore.

        Scribe.RestoreSettings( LoggingSettings.SectionName, ThisLoggingSettings! );

        // validate

        Assert.AreEqual( expectedEventLogDefaultValue, ThisLoggingSettings!.EventLogLogLevels!.CategoryLogLevel!.Default );
        Assert.AreEqual( expectedEventSourceDefaultValue, ThisLoggingSettings!.EventSourceLogLevels!.CategoryLogLevel!.Default );
    }

    #endregion

    #region " values tests "

    [TestMethod()]
    public void LogLevelsForAllProvidersShouldEqual()
    {
        LogLevel expectedDefaultLoggingLevel = LogLevel.Error;
        Assert.IsNotNull( ThisLoggingSettings, nameof( ThisLoggingSettings ) );
        Assert.IsNotNull( ThisLoggingSettings.AllProvidersLogLevels, nameof( LoggingSettings.AllProvidersLogLevels ) );

        Assert.AreEqual( expectedDefaultLoggingLevel, ThisLoggingSettings.AllProvidersLogLevels.Default );

        LogLevel expectedMicrosoftLoggingLevel = LogLevel.Warning;
        Assert.AreEqual( expectedMicrosoftLoggingLevel, ThisLoggingSettings.AllProvidersLogLevels.Microsoft );

        LogLevel expectedCCIsrLoggingLevel = LogLevel.Warning;
        Assert.AreEqual( expectedCCIsrLoggingLevel, ThisLoggingSettings.AllProvidersLogLevels.CCIsr );

    }

    #endregion
}
