using System.Diagnostics;
using cc.isr.Json.AppSettings.ViewModels.Providers;
using cc.isr.Json.AppSettings.ViewModels.Providers.Extensions;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;
/// <summary>
/// (Unit Test Class) an application settings tests using the scribe methods rather than the
/// scribe relay commands.
/// </summary>
/// <remarks>   2023-05-11. </remarks>
[TestClass]
public class TestSiteSettingsTests
{
    #region " construction and cleanup "

    /// <summary>   Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test
    /// in the class.
    /// </remarks>
    /// <param name="testContext">  Gets or sets the test context which provides information about
    ///                             and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        string methodFullName = $"{testContext.FullyQualifiedTestClassName}.{System.Reflection.MethodBase.GetCurrentMethod()?.Name}";
        try
        {
            // get the test site settings with the default values.

            DefaultTestSiteSettings = TestSiteSettings.DefaultInstance;

            // get the test site settings after the values are cleared and then read from the settings file.

            ThisTestSiteSettings = TestSiteSettings.TestInstance;
        }
        catch ( Exception ex )
        {
            if ( Logger is null )
                Trace.WriteLine( $"Failed initializing the test class: {ex}", methodFullName );
            else
                Logger.LogExceptionMultiLineMessage( "Failed initializing the test class:", ex );

            // cleanup to meet strong guarantees

            try
            {
                CleanupTestClass();
            }
            finally
            {
            }
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    { }

    private IDisposable? _loggerScope;

    /// <summary> Initializes the test class instance before each test runs. </summary>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        Console.WriteLine( $"Testing {typeof( Models.AppSettingsScribe ).Assembly.FullName}" );
        this._loggerScope = Logger?.BeginScope( this.TestContext?.TestName ?? string.Empty );
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        this._loggerScope?.Dispose();
    }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    /// <summary>   Gets a logger instance for this category. </summary>
    /// <value> The logger. </value>
    public static ILogger<TestSiteSettingsTests>? Logger { get; } = LoggerProvider.CreateLogger<TestSiteSettingsTests>();

    /// <summary>   Gets the test site settings. </summary>
    /// <value> The test site settings. </value>
    private static TestSiteSettings? ThisTestSiteSettings { get; set; }

    /// <summary>   Gets or sets the default test site settings. </summary>
    /// <value> The default test site settings. </value>
    private static TestSiteSettings? DefaultTestSiteSettings { get; set; }

    #endregion

    #region " initialization tests "

    /// <summary>   (Unit Test Method) tests 00 class initialize. </summary>
    /// <remarks>   2023-04-25. </remarks>
    [TestMethod]
    public void ClassInitializeTest()
    {
        Assert.IsNotNull( ThisTestSiteSettings, $"{nameof( ThisTestSiteSettings )} should initialize" );
        Assert.IsNotNull( DefaultTestSiteSettings, $"{nameof( DefaultTestSiteSettings )} should initialize" );
        Assert.IsNotNull( Logger, $"{nameof( Logger )} should initialize" );
        Assert.IsTrue( Logger.IsEnabled( LogLevel.Information ),
            $"{nameof( Logger )} should be enabled for the {LogLevel.Information} {nameof( LogLevel )}" );

    }

    #endregion

    #region " values tests "

    [TestMethod()]
    public void TestSiteSettingsEnabledShouldRead()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.Enabled, ThisTestSiteSettings!.All );
    }

    [TestMethod()]
    public void TestSiteSettingsAllShould()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.All, ThisTestSiteSettings!.All );
    }

    [TestMethod()]
    public void TestSiteSettingsMessageLevelShouldRead()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.MessageLevel, ThisTestSiteSettings!.MessageLevel );
    }

    [TestMethod()]
    public void TestSiteSettingsIPv4PrefixesShouldRead()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.IPv4Prefixes, ThisTestSiteSettings!.IPv4Prefixes );
    }

    [TestMethod()]
    public void TestSiteSettingsTimeZonesShouldRead()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.TimeZones, ThisTestSiteSettings!.TimeZones );
    }

    [TestMethod()]
    public void TestSiteSettingsTimeZoneOffsetsShouldRead()
    {
        Assert.AreEqual( DefaultTestSiteSettings!.TimeZoneOffsets, ThisTestSiteSettings!.TimeZoneOffsets );
    }

    #endregion
}
