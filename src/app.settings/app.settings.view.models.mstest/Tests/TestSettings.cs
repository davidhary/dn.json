using System.Diagnostics;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Text.Json.Serialization;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary> Application settings tests settings. </summary>
/// <remarks>
/// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2019-02-18. </para>
/// </remarks>
internal sealed class TestSettings : ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    public TestSettings()
    { }

    #endregion

    #region " configuration information "

    private TraceLevel _messageLevel = TraceLevel.Off;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <remarks>
    /// This property name is different from the <see cref="Text.Json"/> property name in
    /// order to ensure that the class is correctly serialized. It's value is initialized as <see cref="TraceLevel.Off"/>
    /// in order to test the reading from the settings file.
    /// </remarks>
    /// <value> The message <see cref="TraceLevel"/>. </value>
    [System.ComponentModel.Description( "Sets the message level" )]
    [JsonPropertyName( "TraceLevel" )]
    public TraceLevel MessageLevel
    {
        get => this._messageLevel;
        set => _ = this.SetProperty( ref this._messageLevel, value );
    }

    private bool _enabled = true;

    /// <summary>   Gets or sets a value indicating whether this object is enabled. </summary>
    /// <value> True if enabled, false if not. </value>
    [System.ComponentModel.Description( "True if testing is enabled for this test class" )]
    public bool Enabled
    {
        get => this._enabled;
        set => this.SetProperty( ref this._enabled, value );
    }

    private bool _all = true;

    /// <summary> Gets or sets all. </summary>
    /// <value> all. </value>
    [System.ComponentModel.Description( "True if all testing is enabled for this test class" )]
    public bool All
    {
        get => this._all;
        set => this.SetProperty( ref this._all, value );
    }

    #endregion

    #region " configuration settings "

    /// <summary>   (Immutable) the expected message level. </summary>
    public const TraceLevel EXPECTED_MESSAGE_LEVEL = TraceLevel.Verbose;

    /// <summary>   The other String value. </summary>
    public const string STRIONG_VALUE_OTHER = "A";

    /// <summary>   The string value expected. </summary>
    public const string STRING_VALUE_EXPECTED = "a";

    /// <summary>   Gets or sets the String value. </summary>
    /// <value> The String value. </value>
    public string StringValue { get; set; } = "a";

    /// <summary>   The other integer value. </summary>
    public const int INTEGER_VALUE_OTHER = 0;

    /// <summary>   The integer value expected. </summary>
    public const int INTEGER_VALUE_EXPECTED = 1;

    /// <summary>   Gets or sets the integer value. </summary>
    /// <value> The integer value. </value>
    public int IntegerValue { get; set; } = 1;

    /// <summary>   The expected Boolean value. </summary>
    public const bool BOOLEAN_VALUE_EXPECTED = true;

    /// <summary>   the other Boolean value. </summary>
    public const bool BOOLEAN_VALUE_OTHER = false;

    /// <summary>   Gets or sets a Boolean value. </summary>
    /// <value> True if Boolean value, false if not. </value>
    public bool BooleanValue { get; set; } = true;

    /// <summary>   The byte value expected. </summary>
    public const byte BYTE_VALUE_EXPECTED = 8;

    /// <summary>   The other byte value. </summary>
    public const byte BYTE_VALUE_OTHER = 0;

    /// <summary>   Gets or sets the byte value. </summary>
    /// <value> The byte value. </value>
    public byte ByteValue { get; set; } = 8;

    /// <summary>   The timespan value expected. </summary>
    public static TimeSpan TimespanValueExpected() { return TimeSpan.Parse( "00:00:02.8250000", System.Globalization.CultureInfo.InvariantCulture ); }

    /// <summary>   Timespan other value. </summary>
    /// <remarks>   David, 2021-01-28. </remarks>
    /// <returns>   A TimeSpan. </returns>
    public static TimeSpan TimespanValueOther() { return TimeSpan.Zero; }

    /// <summary>   Gets or sets the timespan value. </summary>
    /// <value> The timespan value. </value>
    public TimeSpan TimespanValue { get; set; } = TimeSpan.Parse( "00:00:02.8250000", System.Globalization.CultureInfo.InvariantCulture );

    /// <summary>   The date time value expected. </summary>
    public static DateTime DateTimeValueExpected() { return DateTime.Parse( "2020-10-10 10:10:20", System.Globalization.CultureInfo.InvariantCulture ); }

    /// <summary>   The date time other value. </summary>
    public static DateTime DateTimeValueOther() { return DateTime.MinValue; }

    /// <summary>   Gets or sets the date time value. </summary>
    /// <value> The date time value. </value>
    public DateTime DateTimeValue { get; set; } = DateTime.Parse( "2020-10-10 10:10:20", System.Globalization.CultureInfo.InvariantCulture );

    /// <summary>   The trace event type value expected. </summary>
    public const TraceEventType TRACE_EVENT_TYPE_VALUE_EXPECTED = TraceEventType.Information;

    /// <summary>   The other trace event type. </summary>
    public const TraceEventType TRACE_EVENT_TYPE_VALUE_OTHER = TraceEventType.Verbose;

    /// <summary>   Gets or sets the trace event type value. </summary>
    /// <value> The trace event type value. </value>
    public TraceEventType TraceEventTypeValue { get; set; } = TraceEventType.Information;

    #endregion
}
