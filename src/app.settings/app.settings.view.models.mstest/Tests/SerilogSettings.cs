using System.Text.Json.Serialization;
using Serilog.Events;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary>   Defines the Serilog settings as stored in a JSon file. </summary>
/// <remarks>   2023-05-08. </remarks>
public class SerilogSettings
{
    #region " construction "

    /// <summary>
    /// Constructor that prevents a default instance of this class from being created.
    /// </summary>
    /// <remarks>   2023-04-24. </remarks>
    public SerilogSettings()
    {
        this.LevelSwitches = new();
        this.MinimumLevel = new();
        this.PropertiesElement = new();
        this.Providers = [];
        this.Sinks = [];
        this.Enrich = [];
        this.Destructures = [];
    }

    #endregion

    /// <summary>   Gets the name of the section. </summary>
    /// <value> The name of the section. </value>
    [JsonIgnore]
    public static string SectionName => "Serilog";

    /// <summary>   Gets or sets the level switches. </summary>
    /// <value> The level switches. </value>
    [JsonPropertyName( "LevelSwitches" )]
    public SerilogSettingsLevelSwitches? LevelSwitches { get; set; }

    /// <summary>
    /// Gets or sets the minimum level at which events will be passed to sinks. If not specified,
    /// only events at the <see cref="LogEventLevel.Information"/> level and above will be passed
    /// through.
    /// </summary>
    /// <value> The minimum level. </value>
    [JsonPropertyName( "MinimumLevel" )]
    public SerilogSettingsMinimumLevel? MinimumLevel { get; set; }

    /// <summary>   Gets or sets the using. </summary>
    /// <value> The using. </value>
    [JsonPropertyName( "Using" )]
    public List<string>? Providers { get; set; }

    /// <summary>   Gets or sets the sinks that log events will be emitted to.. </summary>
    /// <value> The the sinks that log events will be emitted to.. </value>
    [JsonPropertyName( "WriteTo" )]
    public List<SerilogSettingsSinks>? Sinks { get; set; }

    /// <summary>   Gets or sets enrichment of log events. Enrichers can add, remove and
    /// modify the properties associated with events. </summary>
    /// <value> The enrich. </value>
    [JsonPropertyName( "Enrich" )]
    public List<string>? Enrich { get; set; }

    /// <summary>
    /// Gets or sets the destructures, which configures destructuring of message template parameters.
    /// </summary>
    /// <value>
    /// The destructures, which configures destructuring of message template parameters.
    /// </value>
    [JsonPropertyName( "Destructure" )]
    public List<SerilogSettingsDestructure>? Destructures { get; set; }

    /// <summary>   Gets or sets the properties. </summary>
    /// <value> The properties. </value>
    [JsonPropertyName( "Properties" )]
    public SerilogSettingsProperties? PropertiesElement { get; set; }

    /// <summary>   An arguments. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsArgs
    {
        public SerilogSettingsArgs()
        {
        }

        /// <summary>
        /// Gets or sets the level switch parameters from the <see cref="SerilogSettingsLevelSwitches"/>.
        /// </summary>
        /// <value>
        /// The  level switch parameters from the <see cref="SerilogSettingsLevelSwitches"/>.
        /// </value>
        [JsonPropertyName( "levelSwitch" )]
        public string? LevelSwitch { get; set; }

        /// <summary>   Gets or sets the output template. </summary>
        /// <value> The output template. </value>
        [JsonPropertyName( "outputTemplate" )]
        public string? OutputTemplate { get; set; }

        /// <summary>   Gets or sets the configure. </summary>
        /// <value> The configure. </value>
        [JsonPropertyName( "configure" )]
        public List<SerilogSettingsConfigure>? Configure { get; set; }

        /// <summary>   Gets or sets the hooks. </summary>
        /// <value> The hooks. </value>
        [JsonPropertyName( "hooks" )]
        public string? Hooks { get; set; }

        /// <summary>   Gets or sets the relative path of the logging file. </summary>
        /// <value> The relative path of the logging file. </value>
        [JsonPropertyName( "path" )]
        public string? Path { get; set; }

        /// <summary>   Gets or sets the rolling interval. </summary>
        /// <value> The rolling interval. </value>
        [JsonPropertyName( "rollingInterval" )]
        public string? RollingInterval { get; set; }

        /// <summary>   Gets or sets the retained file count limit. </summary>
        /// <value> The retained file count limit. </value>
        [JsonPropertyName( "retainedFileCountLimit" )]
        public int? RetainedFileCountLimit { get; set; }

        /// <summary>   Gets or sets the maximum depth for object destructuring. </summary>
        /// <value> The maximum depth for object destructuring. </value>
        [JsonPropertyName( "maximumDestructuringDepth" )]
        public int? MaximumDestructuringDepth { get; set; }

        /// <summary>   Gets or sets the maximum string length for object destructuring. </summary>
        /// <value> The maximum  maximum string length for object destructuring. </value>
        [JsonPropertyName( "maximumStringLength" )]
        public int? MaximumStringLength { get; set; }

        /// <summary>   Gets or sets the maximum number of collections for object destructuring. </summary>
        /// <value> The maximum number of collections for object destructuring. </value>
        [JsonPropertyName( "maximumCollectionCount" )]
        public int? MaximumCollectionCount { get; set; }
    }

    /// <summary>   Defines the Serilog settings configure information. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsConfigure
    {
        public SerilogSettingsConfigure() => this.Args = new();

        /// <summary>   Gets or sets the name. </summary>
        /// <value> The name. </value>
        [JsonPropertyName( "Name" )]
        public string? Name { get; set; }

        /// <summary>   Gets or sets the arguments. </summary>
        /// <value> The arguments. </value>
        [JsonPropertyName( "Args" )]
        public SerilogSettingsArgs? Args { get; set; }
    }

    /// <summary>   Defines the Serilog level destructure information. </summary>
    /// <remarks>   2023-05-08. <para> 
    /// Destructuring is the process of taking a complex .NET object and converting it into a structure,
    ///	which may later be represented as say, a JSON object or XML blob.
	/// </para></remarks>
    public class SerilogSettingsDestructure
    {
        public SerilogSettingsDestructure() => this.Args = new();

        /// <summary>   Gets or sets the name. </summary>
        /// <value> The name. </value>
        [JsonPropertyName( "Name" )]
        public string? Name { get; set; }

        /// <summary>   Gets or sets the arguments. </summary>
        /// <value> The arguments. </value>
        [JsonPropertyName( "Args" )]
        public SerilogSettingsArgs? Args { get; set; }
    }

    /// <summary>   Defines the Serilog Settings default log level switches for the various providers. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsLevelSwitches
    {
        /// <summary>   Gets or sets the console log provider log level switch. </summary>
        /// <value> The console log provider log level switch. </value>
        [JsonPropertyName( "$consoleSwitch" )]
        public LogEventLevel? ConsoleSwitch { get; set; }

        /// <summary>   Gets or sets the debug log provider log level switch. </summary>
        /// <value> The debug log provider log level switch. </value>
        [JsonPropertyName( "$debugSwitch" )]
        public LogEventLevel? DebugSwitch { get; set; }

        /// <summary>   Gets or sets the trace log provider log level switch. </summary>
        /// <value> The trace log provider log level switch. </value>
        [JsonPropertyName( "$traceSwitch" )]
        public LogEventLevel? TraceSwitch { get; set; }

        /// <summary>   Gets or sets the file log provider log level switch. </summary>
        /// <value> The file log provider log level switch. </value>
        [JsonPropertyName( "$fileSwitch" )]
        public LogEventLevel? FileSwitch { get; set; }
    }

    /// <summary>
    /// Defines the minimum level at which events will be passed to sinks. If not specified, only
    /// events at the <see cref="LogEventLevel.Information"/> level and above will be passed through.
    /// </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsMinimumLevel
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   2023-05-13. </remarks>
        public SerilogSettingsMinimumLevel() => this.Override = new();

        /// <summary>   Gets or sets the default log level switch. </summary>
        /// <value> The default log level switch. </value>
        [JsonPropertyName( "Default" )]
        public LogEventLevel? Default { get; set; }

        /// <summary>   Gets or sets the overrides of the default <see cref="LogEventLevel"/>. </summary>
        /// <value> The overrides of the default <see cref="LogEventLevel"/>. </value>
        [JsonPropertyName( "Override" )]
        public SerilogSettingsOverride? Override { get; set; }
    }

    /// <summary>   Defines the overrides of the default <see cref="LogEventLevel"/>. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsOverride
    {
        /// <summary>   Gets or sets the default log level switch. </summary>
        /// <value> The default log level switch. </value>
        [JsonPropertyName( "Default" )]
        public LogEventLevel? Default { get; set; }

        /// <summary>   Gets or sets the log level switch for all categories that start with Microsoft. </summary>
        /// <value> The log level switch for all categories that start with Microsoft. </value>
        [JsonPropertyName( "Microsoft" )]
        public LogEventLevel? Microsoft { get; set; }

        /// <summary>   Gets or sets the log level switch for all categories that start with 'Microsoft.Extensions.Hosting". </summary>
        /// <value> The log level switch for all categories that start with 'Microsoft.Extensions.Hosting". </value>
        [JsonPropertyName( "Microsoft.Hosting.Lifetime" )]
        public LogEventLevel? MicrosoftHostingLifetime { get; set; }

        /// <summary>   Gets or sets the log level switch for all categories that start with 'CC.ISR'. </summary>
        /// <value> The log level switch for all categories that start with 'CC.ISR'. </value>
        [JsonPropertyName( "cc.isr" )]
        public LogEventLevel? CCIsr { get; set; }
    }

    /// <summary>   Defines settings settings additional properties. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsProperties
    {
        /// <summary>   Gets or sets the application. </summary>
        /// <value> The application. </value>
        [JsonPropertyName( "Application" )]
        public string? Application { get; set; }
    }

    /// <summary>   A root. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class Root
    {
        /// <value> The Serilog. </value>
        [JsonPropertyName( "Serilog" )]
        public SerilogSettings? Serilog { get; set; }
    }

    /// <summary>   Defines the sinks that log events will be emitted to.. </summary>
    /// <remarks>   2023-05-08. </remarks>
    public class SerilogSettingsSinks
    {
        public SerilogSettingsSinks() => this.Args = new();

        /// <summary>   Gets or sets the name. </summary>
        /// <value> The name. </value>
        [JsonPropertyName( "Name" )]
        public string? Name { get; set; }

        /// <summary>   Gets or sets the arguments. </summary>
        /// <value> The arguments. </value>
        [JsonPropertyName( "Args" )]
        public SerilogSettingsArgs? Args { get; set; }
    }
}
