using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.ViewModels.Bases;

namespace cc.isr.Json.AppSettings.ViewModels.Tests.Services;

/// <summary>   A service for accessing simple unit test dialogs information. </summary>
/// <remarks>   2023-04-29. </remarks>
public class SimpleUnitTestDialogService : IDialogService
{
    /// <summary>
    /// Event queue for all listeners interested in ShowMessageBoxRequested events.
    /// </summary>
    public event EventHandler<ShowMessageBoxResultEventArgs>? ShowMessageBoxRequested;

    /// <summary>   Shows the modal asynchronously. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="registeredController"> The registered controller. </param>
    /// <returns>   The show modal. </returns>
    public async Task<string> ShowModalAsync( ModalViewModelBase registeredController )
    {
        ShowMessageBoxResultEventArgs eArgs = new();
        ShowMessageBoxRequested?.Invoke( this, eArgs );

        return await Task.FromResult( eArgs.ResultButtonText ?? "Cancel" );
    }

    /// <summary>   Sets marshalling context. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="syncContext">  Context for the synchronize. </param>
    public void SetMarshallingContext( object syncContext )
    {
        throw new NotImplementedException();
    }

    /// <summary>   Shows the message box asynchronously. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <exception cref="NullReferenceException">   Thrown when a value was unexpectedly null. </exception>
    /// <param name="title">    The title. </param>
    /// <param name="heading">  The heading. </param>
    /// <param name="message">  The message. </param>
    /// <param name="buttons">  A variable-length parameters list containing buttons. </param>
    /// <returns>   The show message box. </returns>
    public async Task<string> ShowMessageBoxAsync(
        string title, string heading, string message, params string[] buttons )
    {
        ShowMessageBoxResultEventArgs eArgs = new();
        ShowMessageBoxRequested?.Invoke( this, eArgs );

        return eArgs.ResultButtonText is null
            ? throw new InvalidOperationException( "MessageBox test result can't be null." )
            : await Task.FromResult( eArgs.ResultButtonText ?? "Cancel" );
    }
}
