using cc.isr.Json.AppSettings.Services;

namespace cc.isr.Json.AppSettings.ViewModels.Tests.Services;

/// <summary>   A simple test service provider. </summary>
/// <remarks>   2023-04-29. </remarks>
public class SimpleTestServiceProvider : IServiceProvider
{
    #region " singleton "

    /// <summary>   Gets the instance. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <returns>   The instance. </returns>
    public static IServiceProvider GetInstance()
    {
        return _instance.Value;
    }

    private static readonly Lazy<IServiceProvider> _instance = new( () => new SimpleTestServiceProvider(), true );

    #endregion

    /// <summary>   The unit test dialog service. </summary>
    private IDialogService? _unitTestDialogService;

    /// <summary>   Gets the service object of the specified type. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="serviceType">  An object that specifies the type of service object to get. </param>
    /// <returns>
    /// A service object of type <paramref name="serviceType" />.  
    /// 
    ///  -or-  
    /// 
    ///  <see langword="null" /> if there is no service object of type <paramref name="serviceType" />.
    ///  
    /// </returns>
    public object GetService( Type serviceType )
    {
        return serviceType switch
        {
            Type requestedType when typeof( IDialogService ).IsAssignableFrom( requestedType )
                => this._unitTestDialogService ??= new SimpleUnitTestDialogService(),

            Type requestedType when typeof( SimpleUnitTestDialogService ).IsAssignableFrom( requestedType )
                => this._unitTestDialogService ??= new SimpleUnitTestDialogService(),

            _ => throw new NotImplementedException( $"Requested service '{serviceType}' is not supported." )
        };
    }
}
