namespace cc.isr.Json.AppSettings.ViewModels.Tests.Services;

/// <summary>   Additional information for show message box result events. </summary>
/// <remarks>   2023-04-29. </remarks>
public class ShowMessageBoxResultEventArgs : EventArgs
{
    /// <summary>   Constructor. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="resultButtonText"> (Optional) The result button text. </param>
    public ShowMessageBoxResultEventArgs( string? resultButtonText = null ) => this.ResultButtonText = resultButtonText;

    /// <summary>   Gets or sets the result button text. </summary>
    /// <value> The result button text. </value>
    public string? ResultButtonText { get; set; }
}
