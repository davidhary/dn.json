using System.Diagnostics;
using System.Text.Json.Serialization;
using CommunityToolkit.Mvvm.ComponentModel;

namespace cc.isr.Json.AppSettings.ViewModels.Tests;

/// <summary>   Defines the Logging levels as stored in a Json file. </summary>
/// <remarks>   2023-05-08. </remarks>
public class LoggingSettings : ObservableObject
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   2023-05-11. </remarks>
    public LoggingSettings()
    {
        this.TraceLevel = TraceLevel.Off;
        this.AllProvidersLogLevels = new();
        this.ConsoleLogLevel = new();
        this.DebugLogLevels = new();
        this.EventSourceLogLevels = new();
        this.EventLogLogLevels = new();
    }

    #endregion

    /// <summary>   Gets the name of the section. </summary>
    /// <value> The name of the section. </value>
    [JsonIgnore]
    public static string SectionName => "Logging";

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    [JsonPropertyName( "TraceLevel" )]
    public TraceLevel TraceLevel { get; set; }

    /// <summary>   Gets or sets the default log levels. </summary>
    /// <value> The default log levels. </value>
    [JsonPropertyName( "LogLevel" )]
    public LoggingCategoryLogLevel? AllProvidersLogLevels { get; set; }

    /// <summary>   Gets or sets the console log level. </summary>
    /// <value> The console log level. </value>
    [JsonPropertyName( "Console" )]
    public LoggingProviderLogLevel? ConsoleLogLevel { get; set; }

    /// <summary>   Gets or sets the debug log levels. </summary>
    /// <value> The debug log levels. </value>
    [JsonPropertyName( "Debug" )]
    public LoggingProviderLogLevel? DebugLogLevels { get; set; }

    /// <summary>   Gets or sets the event source log levels. </summary>
    /// <value> The event source log levels. </value>
    [JsonPropertyName( "EventSource" )]
    public LoggingProviderLogLevel? EventSourceLogLevels { get; set; }

    /// <summary>   Gets or sets the event log levels. </summary>
    /// <value> The event log levels. </value>
    [JsonPropertyName( "EventLog" )]
    public LoggingProviderLogLevel? EventLogLogLevels { get; set; }
}
/// <summary>   Encapsulates the category log level for a specific provider. </summary>
/// <remarks>   2023-05-08. </remarks>
public class LoggingProviderLogLevel
{
    public LoggingProviderLogLevel() => this.CategoryLogLevel = new();

    /// <summary>   Gets or sets the category log level. </summary>
    /// <value> The category log level. </value>
    [JsonPropertyName( "LogLevel" )]
    public LoggingCategoryLogLevel? CategoryLogLevel { get; set; }
}
/// <summary>   A log levels by categories. </summary>
/// <remarks>   2023-05-08. </remarks>
public class LoggingCategoryLogLevel
{
    /// <summary>   Gets or sets the default log level switch. </summary>
    /// <value> The default minimal log level switch.  </value>
    [JsonPropertyName( "Default" )]
    public LogLevel? Default { get; set; }

    /// <summary>   Gets or sets the log level switch for all categories that start with Microsoft. </summary>
    /// <value> The minimal log level for all categories that start with Microsoft. </value>
    [JsonPropertyName( "Microsoft" )]
    public LogLevel? Microsoft { get; set; }

    /// <summary>   Gets or sets the log level switch for all categories that start with 'CC.ISR'. </summary>
    /// <value> The minimal log level for all categories that start with 'CC.ISR'. </value>
    [JsonPropertyName( "cc.isr" )]
    public LogLevel? CCIsr { get; set; }

    /// <summary>   Gets or sets the log level switch for all categories that start with 'Microsoft.Extensions.Hosting". </summary>
    /// <value> The log level switch for all categories that start with 'Microsoft.Extensions.Hosting". </value>
    [JsonPropertyName( "Microsoft.Extensions.Hosting" )]
    public LogLevel? MicrosoftExtensionsHosting { get; set; }
}
/// <summary>   A root. </summary>
/// <remarks>   2023-05-08. </remarks>
public class Root
{
    /// <summary>   Gets or sets the logging. </summary>
    /// <value> The logging. </value>
    [JsonPropertyName( "Logging" )]
    public LoggingSettings? Logging { get; set; }
}
