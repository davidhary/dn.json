using cc.isr.Json.AppSettings.Services;
using cc.isr.Json.AppSettings.Models;
using cc.isr.Json.AppSettings.ViewModels.Bases;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using cc.isr.Json.AppSettings.Services.Extensions;
using System.ComponentModel;

namespace cc.isr.Json.AppSettings.ViewModels;

/// <summary>   A ViewModel for the application settings editor. </summary>
/// <remarks>   2023-04-29. </remarks>
public partial class AppSettingsEditorViewModel : ViewModelBase
{
    #region " construction "

    /// <summary>   Constructor. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <param name="scribe">           The application settings reader/writer. </param>
    /// <param name="serviceProvider">  The service provider. </param>
    public AppSettingsEditorViewModel( AppSettingsScribe scribe, IServiceProvider serviceProvider ) : base( serviceProvider )
    {
        this._scribe = scribe;
        this._scribe.PropertyChanged += this.HandlePropertyChanged;
    }

    #endregion

    #region " model "

    [ObservableProperty]
    [NotifyCanExecuteChangedFor( nameof( ReadCommand ) )]
    [NotifyCanExecuteChangedFor( nameof( WriteCommand ) )]
    [NotifyCanExecuteChangedFor( nameof( RestoreCommand ) )]
    [NotifyCanExecuteChangedFor( nameof( SelectSettingsCommand ) )]
    [NotifyPropertyChangedFor( nameof( SelectedSettings ) )]
    [NotifyPropertyChangedFor( nameof( SettingsDictionary ) )]
    [NotifyPropertyChangedFor( nameof( SelectedSection ) )]
    private AppSettingsScribe _scribe;

    [ObservableProperty]
    private object? _selectedSettings;

    /// <summary>   Gets or sets the selected application settings section. </summary>
    /// <value> The selected application settings section. </value>
    [ObservableProperty]
    private string? _selectedSection;

    /// <summary>   Gets a dictionary of settings. </summary>
    /// <value> A dictionary of settings. </value>
    private Dictionary<string, object>? SettingsDictionary => this.Scribe?.SettingsDictionary;

    /// <summary>   Handles the property changed. </summary>
    /// <remarks>   2023-05-02. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        Property changed event information. </param>
    private void HandlePropertyChanged( object? sender, PropertyChangedEventArgs e )
    {
        if ( sender is AppSettingsScribe scribe )
        {
            if ( string.Equals( e?.PropertyName, nameof( Models.AppSettingsScribe.SelectedSettings ), StringComparison.OrdinalIgnoreCase ) )
            {
                // the property change event needs to be invoked if changing only settings property rather the settings section.
                if ( string.Equals( this.SelectedSection, scribe.SelectedSection, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.SelectedSettings = scribe.SelectedSettings;
                    this.SelectedSection = scribe.SelectedSection;
                    this.OnPropertyChanged( nameof( AppSettingsScribe.SelectedSettings ) );
                }
                else
                {
                    this.SelectedSettings = scribe.SelectedSettings;
                    this.SelectedSection = scribe.SelectedSection;
                }
            }
        }
    }

    #endregion

    #region " commands "

    /// <summary>   Select settings. </summary>
    /// <remarks>   2023-05-01. </remarks>
    [RelayCommand( CanExecute = nameof( CanSelectSettings ) )]
    public void SelectSettings( string settingsTypeName )
    {
        this.Scribe.SelectSettings( settingsTypeName );
        this.SelectedSettings = this.Scribe.SelectedSettings;
    }

    /// <summary>   Gets a value indicating whether we can select settings. </summary>
    /// <value> True if we can select settings, false if not. </value>
    private bool CanSelectSettings => this.Scribe is not null && this.Scribe.SettingsDictionary.Count > 1;

    /// <summary>   Gets a value indicating whether we can read. </summary>
    /// <value> True if we can read, false if not. </value>
    private bool CanRead => this.Scribe.CanRead;

    /// <summary>   Reads asynchronous. </summary>
    /// <remarks>   2023-05-01. </remarks>
    /// <returns>   A Task. </returns>
    [RelayCommand( CanExecute = nameof( CanRead ), FlowExceptionsToTaskScheduler = true )]
    public async Task ReadAsync()
    {
        await this.Scribe.ReadSettingsAsync();
    }

    /// <summary>   Gets a value indicating whether we can Write. </summary>
    /// <value> True if we can Write, false if not. </value>
    private bool CanWrite => this.Scribe.CanWrite;

    /// <summary>
    /// Writes the application settings using the given synchronization context to notify the View.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <returns>   A Task. </returns>
    [RelayCommand( CanExecute = nameof( CanWrite ), FlowExceptionsToTaskScheduler = true )]
    public async Task WriteAsync()
    {
        await this.Scribe.WriteSettingsAsync();
    }

    /// <summary>   Gets a value indicating whether we can Restore. </summary>
    /// <value> True if we can Restore, false if not. </value>
    private bool CanRestore => this.Scribe.CanRestore;

    /// <summary>
    /// Restores the application settings using the given synchronization context to notify the View.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <returns>   A Task. </returns>
    [RelayCommand( CanExecute = nameof( CanRestore ), FlowExceptionsToTaskScheduler = true )]
    public async Task RestoreAsync()
    {
        string result = await this.UserRestoreQueryAsync();
        if ( string.Equals( result, BUTTON_TEXT_YES, StringComparison.OrdinalIgnoreCase ) )
            await this.Scribe.RestoreSettingsAsync();
    }

    /// <summary>   (Immutable) the yes button text. </summary>
    internal const string BUTTON_TEXT_YES = "Yes";

    /// <summary>   (Immutable) the no button text. </summary>
    internal const string BUTTON_TEXT_NO = "No";

    /// <summary>   User restore query asynchronous. </summary>
    /// <remarks>
    /// 2023-04-29. <para>
    /// This is how the UI is controlled via a Controller or ViewModel: </para><para>
    /// A required Service is set over the <see cref="IServiceProvider"/> which the
    /// Controller/ViewModels got via Dependency Injection. </para><para>
    /// Dependency Injection means, _depending_ on what UI-Stack (or even inside a Unit Test) we're
    /// actually running. </para><para>
    /// The dialogService will do different things: </para>
    /// <list type="bullet">
    /// On WinForms it shows a WinForms MessageBox. <item>
    /// On .NET MAUI, it displays an alert. </item><item>
    /// In the context of a unit test, it doesn't show anything: the unit test, just pretends, the
    /// user had clicked the result button.</item></list>
    /// </remarks>
    /// <returns>   The user restore query. </returns>
    internal async Task<string> UserRestoreQueryAsync()
    {
        IDialogService dialogService = this.ServiceProvider.GetRequiredService<IDialogService>();

        // Now we use this DialogService to remote-control the UI completely
        // _and_ independent of the actual UI technology.
        string buttonString = await dialogService.ShowMessageBoxAsync(
            title: "Restore settings from the Application Context",
            heading: "Do you want to restore settings from the Application Context?",
            message: $"You are about to override the existing settings file {this.Scribe.UserSettingsPath} " +
            $"with the original settings from the Application Context file {this.Scribe.AppContextSettingsPath}. " +
            $"All changes made to the application settings will be lost.",
            buttons: [BUTTON_TEXT_YES, BUTTON_TEXT_NO] );

        return buttonString;
    }

    #endregion
}
