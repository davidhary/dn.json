namespace cc.isr.Json.AppSettings.ViewModels.Bases;

/// <summary>   Additional information for modal view result events. </summary>
/// <remarks>   2023-04-29. </remarks>
/// <remarks>   Constructor. </remarks>
/// <remarks>   2023-04-29. </remarks>
/// <param name="result">   The result. </param>
public class ModalViewResultEventArgs( string result ) : EventArgs
{
    /// <summary>   Gets or sets the result. </summary>
    /// <value> The result. </value>
    public string Result { get; internal set; } = result;
}
