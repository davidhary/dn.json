using CommunityToolkit.Mvvm.Input;

namespace cc.isr.Json.AppSettings.ViewModels.Bases;
/// <summary>
/// A base class for ViewModels which are used in modally shown views that return OK or Cancel <see cref="ModalViewResultEventArgs.Result"/>,
/// which is provided via the dependency injected <see cref="IServiceProvider"/>.
/// </summary>
/// <remarks>   2023-04-29. </remarks>
/// <remarks>   Constructor. </remarks>
/// <remarks>   2023-04-29. </remarks>
/// <param name="serviceProvider">  The service provider. </param>
public abstract partial class ModalViewModelBase( IServiceProvider serviceProvider ) : ViewModelBase( serviceProvider )
{
    /// <summary>   Event queue for all listeners interested in Closed events. </summary>
    public event EventHandler<ModalViewResultEventArgs>? Closed;

    /// <summary>
    /// Handler for the OK Command. Bind this resulting command to the OK Button of a modal view.
    /// </summary>
    /// <remarks>
    /// This raises the <see cref="Closed"/> event, which can be consumed by the UI Service, which in
    /// turn can then pop the view from the view stack of the respected UI. It can then return the
    /// control flow to the caller of the ShowModal method. The "On" prefix is automatically stripped
    /// for the Command name generation.
    /// </remarks>
    [RelayCommand()]
    protected virtual void OnOK()
    {
        Closed?.Invoke( this, new ModalViewResultEventArgs( "OK" ) );
    }

    /// <summary>
    /// Handler for the Cancel command. Bind this resulting command to the Cancel Button of a modal
    /// view.
    /// </summary>
    /// <remarks>   2023-04-29. </remarks>
    [RelayCommand()]
    protected virtual void OnCancel()
    {
        Closed?.Invoke( this, new ModalViewResultEventArgs( "Cancel" ) );
    }
}
