using CommunityToolkit.Mvvm.ComponentModel;

namespace cc.isr.Json.AppSettings.ViewModels.Bases;
/// <summary>
/// A base class for view models that take a <see cref="IServiceProvider">Service Provider</see> via 
/// dependency injection.
/// </summary>
/// <remarks>   2023-04-29. </remarks>
/// <remarks>
/// Creates an instance of this class and initializes the <see cref="ServiceProvider"/> property.
/// </remarks>
/// <remarks>   2023-04-29. </remarks>
/// <param name="serviceProvider">  The service provider. </param>
public abstract class ViewModelBase( IServiceProvider serviceProvider ) : ObservableObject()
{
    /// <summary>   Gets the service provider used to resolve dependencies. </summary>
    /// <value> The service provider. </value>
    public IServiceProvider ServiceProvider { get; } = serviceProvider;
}
