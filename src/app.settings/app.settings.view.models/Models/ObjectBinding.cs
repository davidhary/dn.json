namespace cc.isr.Json.AppSettings.Models;

/// <summary>   Object binding extensions. </summary>
/// <remarks>   2023-05-09. <paras>
/// <see href="https://www.c-sharpCorner.com/UploadFile/ff2f08/deep-copy-of-object-in-C-Sharp/">Deep Copy</see>
/// </paras> </remarks>
public static partial class ObjectExtension
{
    /// <summary>
    /// An object extension method that creates a new object from the given object.
    /// </summary>
    /// <remarks>   2023-05-13. </remarks>
    /// <param name="source">    The objSource to act on. </param>
    /// <returns>   An object. </returns>
    public static object CloneObject( this object source )
    {
        // Get the type of source object and create a new instance of that type

        Type typeSource = source.GetType();
        object target = Activator.CreateInstance( typeSource );
        CopyObject( source, target );
        return target;
    }

    /// <summary>   An object extension method that copies the source properties to the target properties. </summary>
    /// <remarks>   2023-05-13. </remarks>
    /// <param name="source">   The source object. </param>
    /// <param name="destination">   The destination object. </param>
    public static void CopyObject( this object source, object destination )
    {
        if ( source is null || destination is null ) return;
        ObjectExtension.CopyPropsTo<object, object>( source, ref destination );
    }

    /// <summary>
    /// Copies all the matching properties and fields from 'source' to 'destination'.
    /// </summary>
    /// <remarks>
    /// <see href="https://www.ProgrammingNotes.org/7521/cs-how-to-copy-all-properties-fields-from-one-object-to-another-using-cs/"/>
    /// </remarks>
    /// <typeparam name="T1">   Generic type parameter. </typeparam>
    /// <typeparam name="T2">   Generic type parameter. </typeparam>
    /// <param name="source">       The source object to copy from. </param>
    /// <param name="destination">  [in,out] The destination object to copy to. </param>
    public static void CopyPropsTo<T1, T2>( this T1 source, ref T2 destination )
    {
        if ( source is null || destination is null ) return;
        List<System.Reflection.MemberInfo> sourceMembers = GetMembers( source.GetType() );
        List<System.Reflection.MemberInfo> destinationMembers = GetMembers( destination.GetType() );

        // Copy data from source to destination

        foreach ( System.Reflection.MemberInfo sourceMember in sourceMembers )
        {
            if ( !CanRead( sourceMember ) )
            {
                continue;
            }
            System.Reflection.MemberInfo destinationMember = destinationMembers.FirstOrDefault( x => x.Name.ToLower() == sourceMember.Name.ToLower() );
            if ( destinationMember == null || !CanWrite( destinationMember ) )
            {
                continue;
            }
            SetObjectValue( ref destination, destinationMember, GetMemberValue( source, sourceMember ) );
        }
    }

    private static void SetObjectValue<T>( ref T obj, System.Reflection.MemberInfo member, object? value )
    {
        // Boxing method used for modifying structures

        object boxed = obj!.GetType().IsValueType ? ( object ) obj : obj;
        SetMemberValue( ref boxed, member, value );
        obj = ( T ) boxed;
    }

    private static void SetMemberValue<T>( ref T obj, System.Reflection.MemberInfo member, object? value )
    {
        if ( IsProperty( member ) )
        {
            System.Reflection.PropertyInfo prop = ( System.Reflection.PropertyInfo ) member;
            if ( prop.SetMethod is not null )
            {
                prop.SetValue( obj, value );
            }
        }
        else if ( IsField( member ) )
        {
            System.Reflection.FieldInfo field = ( System.Reflection.FieldInfo ) member;
            field.SetValue( obj, value );
        }
    }

    private static object? GetMemberValue( object obj, System.Reflection.MemberInfo member )
    {
        object? result = null;
        if ( IsProperty( member ) )
        {
            System.Reflection.PropertyInfo prop = ( System.Reflection.PropertyInfo ) member;
            result = prop.GetValue( obj, prop.GetIndexParameters().Count() == 1 ? new object?[] { null } : null );
        }
        else if ( IsField( member ) )
        {
            System.Reflection.FieldInfo field = ( System.Reflection.FieldInfo ) member;
            result = field.GetValue( obj );
        }
        return result;
    }

    private static bool CanWrite( System.Reflection.MemberInfo member )
    {
        return IsProperty( member ) ? (( System.Reflection.PropertyInfo ) member).CanWrite : IsField( member );
    }

    private static bool CanRead( System.Reflection.MemberInfo member )
    {
        return IsProperty( member ) ? (( System.Reflection.PropertyInfo ) member).CanRead : IsField( member );
    }

    private static bool IsProperty( System.Reflection.MemberInfo member )
    {
        return IsType( member.GetType(), typeof( System.Reflection.PropertyInfo ) );
    }

    private static bool IsField( System.Reflection.MemberInfo member )
    {
        return IsType( member.GetType(), typeof( System.Reflection.FieldInfo ) );
    }

    private static bool IsType( Type type, Type targetType )
    {
        return type.Equals( targetType ) || type.IsSubclassOf( targetType );
    }

    private static List<System.Reflection.MemberInfo> GetMembers( Type type )
    {
        System.Reflection.BindingFlags flags = System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public
            | System.Reflection.BindingFlags.NonPublic;
        List<System.Reflection.MemberInfo> members = [.. type.GetProperties( flags ), .. type.GetFields( flags )];
        return members;
    }
}
