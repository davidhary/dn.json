using System.Text.Json.Serialization;
using System.Text.Json;
using CommunityToolkit.Mvvm.ComponentModel;

namespace cc.isr.Json.AppSettings.Models;

/// <summary>   A reader and writer of a collection of <see cref="System.Text.Json"/> application settings. </summary>
/// <remarks>   2023-04-26. </remarks>
public partial class AppSettingsScribe : ObservableObject
{
    #region " contruction "

    /// <summary>   Constructor. </summary>
    /// <remarks>
    /// 2023-04-26. <para>
    /// Selects the initial <see cref="SelectedSettings"/> from the first item of <paramref name="settings"/>,
    /// sets the file i/o capabilities <see cref="CanRead"/>, <see cref="CanWrite"/> and <see cref="CanRestore"/>
    /// and initializes the <see cref="UserSettingsPath"/> settings if empty. </para><para>
    /// Builds the <see cref="_settingsDictionary"/> using the settings object <see cref="object.GetType"/>.
    /// Name as the section name. </para>
    /// </remarks>
    /// <param name="settings">                 The array of settings instances. </param>
    /// <param name="appContextSettingsPath">   The full name of the application context settings
    ///                                         file. </param>
    /// <param name="userSettingsPath">         The file name of the user settings file. </param>
    public AppSettingsScribe( object[] settings, string appContextSettingsPath, string userSettingsPath )
        : this( [.. BuildSettingsSections( settings )], settings, appContextSettingsPath, userSettingsPath ) { }

    ///<summary>   Constructor. </summary>
    /// <remarks>
    /// 2023-04-26. <para>
    /// Selects the initial <see cref="SelectedSettings"/> from the first item of <paramref name="sections"/>,
    /// sets the file i/o capabilities <see cref="CanRead"/>, <see cref="CanWrite"/> and <see cref="CanRestore"/>
    /// and initializes the <see cref="UserSettingsPath"/> settings if empty. </para><para>
    /// Builds the <see cref="_settingsDictionary"/> using the settings sections as keys.
    /// </para>
    /// </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <exception cref="ArgumentException">        Thrown when one or more arguments have
    ///                                             unsupported or illegal values. </exception>
    /// <param name="sections">                 The array of section names corresponding to each
    ///                                         settings. </param>
    /// <param name="settings">                 The array of settings instances. </param>
    /// <param name="appContextSettingsPath">   The full name of the application context settings
    ///                                         file. </param>
    /// <param name="userSettingsPath">         The file name of the user settings file. </param>
    public AppSettingsScribe( string[] sections, object[] settings, string appContextSettingsPath, string userSettingsPath )
    {
        this._settingsDictionary = BuildSettingsDictionary( sections, settings );
        if ( string.IsNullOrWhiteSpace( appContextSettingsPath ) ) throw new ArgumentNullException( nameof( appContextSettingsPath ) );
        if ( string.IsNullOrWhiteSpace( userSettingsPath ) ) throw new ArgumentNullException( nameof( userSettingsPath ) );
        if ( !File.Exists( appContextSettingsPath ) ) throw new ArgumentException( $"Application context settings file {appContextSettingsPath} must exist",
            nameof( appContextSettingsPath ) );

        this._selectedSettings = new();
        this._selectedSection = string.Empty;
        this.SelectSettings( sections[0] );
        this.AppContextSettingsPath = appContextSettingsPath;

        // set the user settings path

        this.SetUserSettingsPath( userSettingsPath );

        // read the settings

        this.ReadSettings();
    }

    /// <summary>   Sets the full name of the user settings file. </summary>
    /// <remarks>   2023-05-08. </remarks>
    /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
    ///                                                 are null. </exception>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <param name="userSettingsPath"> Full name of the user settings file. </param>
    [System.Diagnostics.CodeAnalysis.MemberNotNull( nameof( UserSettingsPath ) )]
    public void SetUserSettingsPath( string userSettingsPath )
    {
        if ( string.IsNullOrWhiteSpace( userSettingsPath ) ) throw new ArgumentNullException( nameof( userSettingsPath ) );

        this.UserSettingsPath = userSettingsPath;


        // determine access to the app context settings file.
        (bool canReadAppContextSettings, bool canWriteAppContextSettings) = GetFileAccess( this.AppContextSettingsPath, FileMode.Open );

        // set minimum length to two curly brackets plus two empty lines.

        int minimumLength = 16;

        if ( string.Equals( this.AppContextSettingsPath, this.UserSettingsPath, StringComparison.OrdinalIgnoreCase ) )
        {
            // if we have a single settings files, we might be able to read and write but not restore

            this.CanRead = canReadAppContextSettings;
            this.CanWrite = canWriteAppContextSettings;
            this.CanRestore = false;
        }
        else
        {
            if ( !File.Exists( this.UserSettingsPath ) || (new FileInfo( this.UserSettingsPath ).Length < minimumLength) )
            {
                if ( canReadAppContextSettings )
                {
                    (_, bool canWrite) = GetFileAccess( this.UserSettingsPath, FileMode.Create );
                    if ( canWrite )
                        // update the user file if new
                        File.WriteAllText( this.UserSettingsPath, File.ReadAllText( this.AppContextSettingsPath ) );
                }
            }

            // we might be able to read, write and restore.
            (bool canReadUserContext, bool canWriteUserContext) = GetFileAccess( this.UserSettingsPath, FileMode.Open );
            this.CanRead = canReadUserContext && (new FileInfo( this.UserSettingsPath ).Length > minimumLength);
            this.CanWrite = canWriteUserContext;
            this.CanRestore = canReadAppContextSettings && this.CanWrite;
        }
    }

    #endregion

    #region " file i/o capabilities "

    /// <summary>   Gets file access. </summary>
    /// <remarks>   2023-04-27. </remarks>
    /// <param name="path">     Full pathname of the file. </param>
    /// <param name="fileMode"> The file mode. </param>
    /// <returns>   The file access. </returns>
    private static (bool canRead, bool canWrite) GetFileAccess( string path, FileMode fileMode )
    {
        FileInfo? fi = new( path );
        if ( fi is null || fi.Directory is null ) return (false, false);
        if ( !fi.Directory.Exists )
        {
            try
            {
                fi.Directory.Create();
            }
            catch
            {
                return (false, false);
            }
        }
        using FileStream fileStream = new( path, fileMode );
        return (fileStream.CanRead, fileStream.CanWrite);
    }

    /// <summary>
    /// Gets or sets a value indicating whether we can write the settings to the <see cref="UserSettingsPath"/>.
    /// </summary>
    /// <value>
    /// True if we can write the settings to the <see cref="UserSettingsPath"/>, false
    /// if not.
    /// </value>
    [ObservableProperty]
    private bool _canWrite;

    /// <summary>
    /// Gets or sets a value indicating whether we can read the settings from the <see cref="AppContextSettingsPath"/>.
    /// </summary>
    /// <value>
    /// True if we can read the settings from the <see cref="AppContextSettingsPath"/>, false
    /// if not.
    /// </value>
    [ObservableProperty]
    private bool _canRead;

    /// <summary>
    /// Gets or sets a value indicating whether we can restore the settings by reading from the <see cref="AppContextSettingsPath"/>
    /// and writing to the <see cref="UserSettingsPath"/>.
    /// </summary>
    /// <value>
    /// True if we can restore the settings by reading from the <see cref="AppContextSettingsPath"/>
    /// and writing to the <see cref="UserSettingsPath"/>, false if not.
    /// </value>
    [ObservableProperty]
    private bool _canRestore;

    #endregion

    #region " json settings "

    /// <summary>   Gets or sets the selected application settings to edit. </summary>
    /// <value> The application settings. </value>
    [ObservableProperty]
    private object _selectedSettings;

    /// <summary>   Gets or sets the selected application settings section. </summary>
    /// <value> The selected application settings section. </value>
    [ObservableProperty]
    private string _selectedSection;

    /// <summary>
    /// Select settings using the settings section name as a key to the dictionary of settings.
    /// </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <param name="sectionName"> Name of the settings section; this might be the settings object type. </param>
    public void SelectSettings( string sectionName )
    {
        if ( this.SettingsDictionary.TryGetValue( sectionName, out object? selectedSettings ) )
        {
            // ensure getting notified on property change as we are not notified on change of individual properties.
            if ( string.Equals( this.SelectedSection, sectionName, StringComparison.OrdinalIgnoreCase ) )
            {
                this.SelectedSettings = selectedSettings;
                this.SelectedSection = sectionName;
                this.OnPropertyChanged( nameof( AppSettingsScribe.SelectedSettings ) );
                this.OnPropertyChanged( nameof( AppSettingsScribe.SelectedSection ) );
            }
            else
            {
                this.SelectedSettings = selectedSettings;
                this.SelectedSection = sectionName;
            }
        }
    }

    #endregion

    #region " settings files "

    /// <summary>
    /// Gets or sets the full name of the application context settings file.
    /// </summary>
    /// <value> The full name of the application context settings file. </value>
    public string AppContextSettingsPath { get; private set; }

    /// <summary>   Gets or sets the full name of the user settings file. </summary>
    /// <value> The full name of the user settings file. </value>
    public string UserSettingsPath { get; private set; }

    /// <summary>   Gets or sets the filename of all users settings full file. </summary>
    /// <value> The filename of all users settings full file. </value>
    public string? AllUsersSettingsPath { get; set; }

    /// <summary>   Gets or sets the filename of this user settings full file. </summary>
    /// <value> The filename of this user settings full file. </value>
    public string? ThisUserSettingsPath { get; set; }

    #endregion

    #region " read and write all settings "

    /// <summary>   Copies the settings. </summary>
    /// <remarks>   2023-05-11. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="source"> Full pathname of source file. </param>
    /// <param name="destination">   Full pathname of the destination file. </param>
    public static void CopySettings( string source, string destination )
    {
        if ( !File.Exists( source ) )
            throw new FileNotFoundException( $"Settings file {source} not found", source );

        FileInfo fi = new( destination );
        if ( !fi.Directory!.Exists )
            fi.Directory.Create();

        File.WriteAllText( destination, File.ReadAllText( source ) );
    }

    /// <summary>   Reads the settings using serialization and object binding. </summary>
    /// <remarks>   2023-04-25. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="path">                 Full name of the settings file. </param>
    /// <param name="settingsDictionary">   A dictionary of settings objects keyed by unique JSon
    ///                                     section names. </param>
    public static void ReadSettings( string path, Dictionary<string, object> settingsDictionary )
    {
        if ( !File.Exists( path ) )
            throw new FileNotFoundException( $"Settings file {path} not found", path );

        JsonDocumentOptions documentOptions = new()
        {
            AllowTrailingCommas = true
        };

        JsonSerializerOptions serializerOptions = new()
        {
            WriteIndented = true,
            Converters = { new JsonStringEnumConverter( JsonNamingPolicy.CamelCase ) }
        };

        using FileStream stream = File.OpenRead( path );
#if false
        using JsonDocument document = JsonDocument.Parse( stream, documentOptions );
        Dictionary<string, object>? dix = JsonSerializer.Deserialize<Dictionary<string, object>>( document, serializerOptions );
        if ( dix is not null )
        {
            foreach ( var item in dix )
            {
                // presently, thuis method does not use Json property names for binding and thus fails.
                item.Value.BindObject( settingsDictionary[item.Key] );
            }
        }
#else
        using JsonDocument document = JsonDocument.Parse( stream, documentOptions );
        foreach ( KeyValuePair<string, object> sectionSettings in settingsDictionary )
        {
            object? section = document.RootElement.GetProperty( sectionSettings.Key ).Deserialize( sectionSettings.Value.GetType(), serializerOptions );
            section?.CopyObject( sectionSettings.Value );
        }
#endif
    }

    /// <summary>   Writes all settings. </summary>
    /// <remarks>   2023-04-25. </remarks>
    /// <param name="path">                 Full name of the settings file. </param>
    /// <param name="settingsDictionary">   A dictionary of settings objects keyed by unique JSon
    ///                                     section names. </param>
    public static void WriteSettings( string path, Dictionary<string, object> settingsDictionary )
    {
        JsonSerializerOptions serializerOptions = new()
        {
            WriteIndented = true,
            Converters = { new JsonStringEnumConverter( JsonNamingPolicy.CamelCase ) }
        };
        FileInfo fi = new( path );
        if ( !fi.Directory!.Exists )
            fi.Directory.Create();
        File.WriteAllText( path, JsonSerializer.Serialize( settingsDictionary, serializerOptions ) );
    }

    /// <summary>   Restore settings. </summary>
    /// <remarks>   2023-04-25. </remarks>
    /// <param name="sourcePath">           Full name of the source settings file. </param>
    /// <param name="destinationPath">      Full name of the destination settings file. </param>
    /// <param name="settingsDictionary">   A dictionary of settings objects keyed by unique JSon
    ///                                     section names. </param>
    public static void RestoreSettings( string sourcePath, string destinationPath, Dictionary<string, object> settingsDictionary )
    {
        // copy the settings from the source to the destination

        CopySettings( sourcePath, destinationPath );

        // read the settings from the destination path.

        AppSettingsScribe.ReadSettings( destinationPath, settingsDictionary );

    }

    #endregion

    #region " read and write a single settings "

    /// <summary>   Reads a settings using serialization and object binding. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="path">         Full name of the settings file. </param>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public static void ReadSettings( string path, string sectionName, object settings )
    {
        if ( !File.Exists( path ) )
            throw new FileNotFoundException( $"Settings file {path} not found", path );

        JsonSerializerOptions serializerOptions = new()
        {
            WriteIndented = true,
            Converters = { new JsonStringEnumConverter( JsonNamingPolicy.CamelCase ) }
        };
        using FileStream stream = File.OpenRead( path );
        // Dictionary<string, object>? dix = JsonSerializer.Deserialize<Dictionary<string, object>>( stream, serializerOptions );
        // dix?[sectionName].BindObject( settings );

        JsonDocumentOptions documentOptions = new()
        {
            AllowTrailingCommas = true
        };

        using JsonDocument document = JsonDocument.Parse( stream, documentOptions );
        object? section = document.RootElement.GetProperty( sectionName ).Deserialize( settings.GetType(), serializerOptions );
        section?.CopyObject( settings );
    }

    /// <summary>   Reads application context settings. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public void ReadAppContextSettings( string sectionName, object settings )
    {
        AppSettingsScribe.ReadSettings( this.AppContextSettingsPath, sectionName, settings );
        if ( string.Equals( sectionName, this.SelectedSection, StringComparison.OrdinalIgnoreCase ) )
            this.SelectSettings( this.SelectedSection );
    }

    /// <summary>   Reads settings synchronously. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public void ReadSettings( string sectionName, object settings )
    {
        if ( System.IO.File.Exists( this.UserSettingsPath ) )
            AppSettingsScribe.ReadSettings( this.UserSettingsPath, sectionName, settings );
        else
            AppSettingsScribe.RestoreSettings( this.AppContextSettingsPath, this.UserSettingsPath, sectionName, settings );
        if ( string.Equals( sectionName, this.SelectedSection, StringComparison.OrdinalIgnoreCase ) )
            this.SelectSettings( this.SelectedSection );
    }

    /// <summary>   Reads settings asynchronously. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    /// <returns>   A Task. </returns>
    public async Task ReadSettingsAsync( string sectionName, object settings )
    {
        await Task.Factory.StartNew( () => this.ReadSettings( sectionName, settings ) );
    }

    /// <summary>   Writes all settings. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <exception cref="FileNotFoundException">    Thrown when the requested file is not present. </exception>
    /// <param name="path">         Full name of the settings file. </param>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public static void WriteSettings( string path, string sectionName, object settings )
    {
        JsonSerializerOptions serializerOptions = new()
        {
            WriteIndented = true,
            Converters = { new JsonStringEnumConverter( JsonNamingPolicy.CamelCase ) }
        };

        Dictionary<string, object>? dix;
        if ( !File.Exists( path ) )
        {
            dix = new() { { sectionName, settings } };
        }
        else
        {
            using FileStream stream = File.OpenRead( path );
            dix = JsonSerializer.Deserialize<Dictionary<string, object>>( stream, serializerOptions );
            if ( dix is not null )
                dix[sectionName] = settings;
            stream.Dispose();
        }
        File.WriteAllText( path, JsonSerializer.Serialize( dix, serializerOptions ) );
    }

    /// <summary>   Writes settings synchronously. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public void WriteSettings( string sectionName, object settings )
    {
        AppSettingsScribe.WriteSettings( this.UserSettingsPath, sectionName, settings );
    }

    /// <summary>   Writes the settings asynchronously. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    /// <returns>   A Task. </returns>
    public async Task WriteSettingsAsync( string sectionName, object settings )
    {
        await Task.Factory.StartNew( () => this.WriteSettings( sectionName, settings ) );
    }

    /// <summary>   Restore settings. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sourcePath">       Full name of the source settings file. </param>
    /// <param name="destinationPath">  Full name of the destination settings file. </param>
    /// <param name="sectionName">      Name of the settings section; this might be the settings
    ///                                 object type. </param>
    /// <param name="settings">         The array of settings instances. </param>
    public static void RestoreSettings( string sourcePath, string destinationPath, string sectionName, object settings )
    {
        // read the settings from the application context file.

        AppSettingsScribe.ReadSettings( sourcePath, sectionName, settings );

        // write the settings to the user settings file

        AppSettingsScribe.WriteSettings( destinationPath, sectionName, settings );

    }

    /// <summary>   Restore settings synchronously. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    public void RestoreSettings( string sectionName, object settings )
    {
        this.ReadAppContextSettings( sectionName, settings );
        this.WriteSettings( sectionName, settings );
    }

    /// <summary>   Restore settings asynchronous. </summary>
    /// <remarks>   2023-05-10. </remarks>
    /// <param name="sectionName">  Name of the settings section; this might be the settings object
    ///                             type. </param>
    /// <param name="settings">     The array of settings instances. </param>
    /// <returns>   A Task. </returns>
    public async Task RestoreSettingsAsync( string sectionName, object settings )
    {
        await Task.Factory.StartNew( () => this.RestoreSettings( sectionName, settings ) );
    }

    #endregion

    #region " test site settings "

    /// <summary>   Gets or sets a dictionary of settings. </summary>
    /// <value> A dictionary of settings. </value>
    [JsonIgnore]
    [ObservableProperty]
    private Dictionary<string, object> _settingsDictionary;

    /// <summary>   Builds settings dictionary. </summary>
    /// <remarks>   2023-05-08. </remarks>
    /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
    ///                                                 are null. </exception>
    /// <exception cref="ArgumentException">            Thrown when one or more arguments have
    ///                                                 unsupported or illegal values. </exception>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <param name="sections"> The sections. </param>
    /// <param name="settings"> Instances of settings classes. </param>
    /// <returns>   A Dictionary{string,object} of setting sections and setting instances </returns>
    public static Dictionary<string, object> BuildSettingsDictionary( string[] sections, object[] settings )
    {
        if ( sections is null ) throw new ArgumentNullException( nameof( sections ) );
        if ( settings.Length == 0 ) throw new ArgumentException( $"{nameof( sections )} must include at least one section", nameof( sections ) );
        if ( settings is null ) throw new ArgumentNullException( nameof( settings ) );
        if ( settings.Length == 0 ) throw new ArgumentException( $"{nameof( settings )} must include at least one settings", nameof( settings ) );
        if ( sections.Length != settings.Length ) throw new InvalidOperationException( $"The number of sections {sections.Length} must match the number of settings {settings.Length}" );
        Dictionary<string, object> dix = [];
        for ( int i = 0; i < sections.Length; i++ )
            dix.Add( sections[i], settings[i] );
        return dix;
    }

    /// <summary>   Builds settings dictionary. </summary>
    /// <remarks>   2023-05-08. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <exception cref="ArgumentException">        Thrown when one or more arguments have
    ///                                             unsupported or illegal values. </exception>
    /// <param name="settings"> Instances of settings classes. </param>
    /// <returns>   A Dictionary{string,object} of setting sections and setting instances. </returns>
    public static Dictionary<string, object> BuildSettingsDictionary( object[] settings )
    {
        if ( settings is null ) throw new ArgumentNullException( nameof( settings ) );
        if ( settings.Length == 0 ) throw new ArgumentException( $"{nameof( settings )} must include at least one settings", nameof( settings ) );
        List<string> sections = [];
        foreach ( object setting in settings )
            sections.Add( setting.GetType().Name );
        return AppSettingsScribe.BuildSettingsDictionary( [.. sections], settings );
    }

    /// <summary>   Builds settings sections. </summary>
    /// <remarks>   2023-05-08. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <exception cref="ArgumentException">        Thrown when one or more arguments have
    ///                                             unsupported or illegal values. </exception>
    /// <param name="settings"> The array of settings instances. </param>
    /// <returns>   A <see cref="List{T}" /> of <see cref="string"/>.  </returns>
    public static List<string> BuildSettingsSections( object[] settings )
    {
        if ( settings is null ) throw new ArgumentNullException( nameof( settings ) );
        if ( settings.Length == 0 ) throw new ArgumentException( $"{nameof( settings )} must include at least one settings", nameof( settings ) );
        List<string> sections = [];
        foreach ( object setting in settings )
            sections.Add( setting.GetType().Name );
        return sections;
    }

    #endregion

    #region " read and write async "

    /// <summary>
    /// Copy settings from the <see cref="AppContextSettingsPath"/> to the <see cref="UserSettingsPath"/>.
    /// </summary>
    /// <remarks>   2023-05-01. </remarks>
    public void CopySettings()
    {
        AppSettingsScribe.CopySettings( this.AppContextSettingsPath, this.UserSettingsPath );
    }

    /// <summary>   Reads application context settings. </summary>
    /// <remarks>   2023-05-02. </remarks>
    public void ReadAppContextSettings()
    {
        AppSettingsScribe.ReadSettings( this.AppContextSettingsPath, this.SettingsDictionary );
        this.SelectSettings( this.SelectedSection );
    }

    /// <summary>   Reads settings synchronously. </summary>
    /// <remarks>   2023-05-01. </remarks>
    public void ReadSettings()
    {
        if ( System.IO.File.Exists( this.UserSettingsPath ) )
            AppSettingsScribe.ReadSettings( this.UserSettingsPath, this.SettingsDictionary );
        else
            AppSettingsScribe.RestoreSettings( this.AppContextSettingsPath, this.UserSettingsPath, this.SettingsDictionary );
        this.SelectSettings( this.SelectedSection );
    }

    /// <summary>   Reads settings asynchronously. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <returns>   A Task. </returns>
    public async Task ReadSettingsAsync()
    {
        await Task.Factory.StartNew( this.ReadSettings );
    }

    /// <summary>   Writes settings synchronously. </summary>
    /// <remarks>   2023-04-26. </remarks>
    public void WriteSettings()
    {
        AppSettingsScribe.WriteSettings( this.UserSettingsPath, this.SettingsDictionary );
    }

    /// <summary>   Writes the settings asynchronously. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <returns>   A Task. </returns>
    public async Task WriteSettingsAsync()
    {
        await Task.Factory.StartNew( this.WriteSettings );
    }

    /// <summary>   Restore settings synchronously. </summary>
    /// <remarks>   2023-05-01. </remarks>
    public void RestoreSettings()
    {
        AppSettingsScribe.RestoreSettings( this.AppContextSettingsPath, this.UserSettingsPath, this.SettingsDictionary );
        this.SelectSettings( this.SelectedSection );
    }

    /// <summary>   Restore settings asynchronous. </summary>
    /// <remarks>   2023-04-26. </remarks>
    /// <returns>   A Task. </returns>
    public async Task RestoreSettingsAsync()
    {
        await Task.Factory.StartNew( this.RestoreSettings );
    }

    #endregion
}
