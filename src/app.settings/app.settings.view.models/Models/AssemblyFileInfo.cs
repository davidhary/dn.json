using System.Diagnostics;
using System.Reflection;

namespace cc.isr.Json.AppSettings.Models;

/// <summary>   Information about the assembly file names. </summary>
/// <remarks>   2023-05-09. </remarks>
public class AssemblyFileInfo
{
    #region " contruction "

    /// <summary>   Constructor. </summary>
    /// <remarks>   2023-05-09. </remarks>
    /// <param name="assembly">     The assembly. </param>
    /// <param name="fileTitle">    (Optional) The file name title. </param>
    /// <param name="suffix">       (Optional) The file name suffix to append to the title. </param>
    /// <param name="extension">    (Optional) The file name extension. </param>
    public AssemblyFileInfo( Assembly assembly, string? fileTitle = null, string? suffix = ".Settings", string extension = ".json" )
    {
        fileTitle = string.IsNullOrEmpty( fileTitle ) ? assembly.GetName().Name : fileTitle;
        this.FileName = $"{fileTitle}{suffix}{extension}";
        this.AssemblyFileVersionInfo = FileVersionInfo.GetVersionInfo( assembly.Location );
        this.ProductFolder = Path.Combine( this.AssemblyFileVersionInfo.CompanyName!, this.AssemblyFileVersionInfo.ProductName!, this.AssemblyFileVersionInfo.ProductVersion! );
        this.BaseFolder = AppContext.BaseDirectory;
        this.AllUsersFolder = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.CommonApplicationData ) );
        this.ThisUserFolder = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ) );
        this.AllUsersAssemblyProductFolder = Path.Combine( this.AllUsersFolder, this.ProductFolder );
        this.ThisUserAssemblyProductFolder = Path.Combine( this.ThisUserFolder, this.ProductFolder );
        this.AppContextAssemblyFilePath = Path.Combine( this.BaseFolder, this.FileName );
        this.AllUsersAssemblyFilePath = Path.Combine( this.AllUsersFolder, this.ProductFolder, this.FileName );
        this.ThisUserAssemblyFilePath = Path.Combine( this.ThisUserFolder, this.ProductFolder, this.FileName );
    }

    #endregion

    #region " file name builders "

    /// <summary>
    /// Builds product folder, such as <see cref="FileVersionInfo.CompanyName"/>/<see cref="FileVersionInfo.ProductName"/>
    /// /<see cref="FileVersionInfo.ProductVersion"/>.
    /// </summary>
    /// <remarks>   2023-05-11. </remarks>
    /// <param name="assembly"> The assembly. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildProductFolder( Assembly assembly )
    {
        return BuildProductFolder( FileVersionInfo.GetVersionInfo( assembly.Location ) );
    }

    /// <summary>
    /// Builds product folder, such as <see cref="FileVersionInfo.CompanyName"/>/<see cref="FileVersionInfo.ProductName"/>
    /// /<see cref="FileVersionInfo.ProductVersion"/>.
    /// </summary>
    /// <remarks>   2023-05-11. </remarks>
    /// <param name="assemblyFileVersionInfo">  Information describing the assembly file version. </param>
    /// <returns>   A <see cref="string" />. </returns>
    public static string BuildProductFolder( FileVersionInfo assemblyFileVersionInfo )
    {
        return Path.Combine( assemblyFileVersionInfo.CompanyName!, assemblyFileVersionInfo.ProductName!, assemblyFileVersionInfo.ProductVersion! );
    }

    #endregion

    #region " assembly file info "

    /// <summary>    Gets information describing the assembly file version. </summary>
    /// <value>  Information describing the assembly file version. </value>
    public FileVersionInfo? AssemblyFileVersionInfo { get; }

    /// <summary>   Gets the name of the assembly product folder consisting of the <see cref="FileVersionInfo.CompanyName"/>
    /// <see cref="FileVersionInfo.ProductName"/> and <see cref="FileVersionInfo.ProductVersion"/>. </summary>
    /// <value> The name of the product file. </value>
    public string ProductFolder { get; }

    /// <summary>   Gets the full pathname of the application base folder. </summary>
    /// <value> The full pathname of the application base folder. </value>
    public string BaseFolder { get; }

    /// <summary>   Gets the full pathname of the common writable folder shared by all users. </summary>
    /// <value> The full pathname of the folder shared by all users. </value>
    public string AllUsersFolder { get; }

    /// <summary>   Gets or sets the full pathname of the writable folder for this user. </summary>
    /// <value> The full pathname of the writable folder for this user. </value>
    public string ThisUserFolder { get; }

    /// <summary>   Gets the full pathname of the assembly product folder shared by all users. </summary>
    /// <value> The full pathname of the assembly product folder shared by all users. </value>
    public string AllUsersAssemblyProductFolder { get; }

    /// <summary>   Gets or sets the full pathname of assembly product folder for this user. </summary>
    /// <value> The full pathname of the assembly product folder for this user. </value>
    public string ThisUserAssemblyProductFolder { get; }

    #endregion

    #region " assemply application and user files "

    /// <summary>   Gets the file name of the assembly file. </summary>
    /// <value> The file name of the assembly file. </value>
    public string FileName { get; }

    /// <summary>
    /// Gets or sets the full name of the application context assembly file.
    /// </summary>
    /// <value> The full name of the application context assembly file. </value>
    public string? AppContextAssemblyFilePath { get; }

    /// <summary>   Gets or sets the full name of the assembly file for all users. </summary>
    /// <value> The full name of the assembly file for all users. </value>
    public string? AllUsersAssemblyFilePath { get; }

    /// <summary>   Gets or sets the full name of the assembly file for this user. </summary>
    /// <value> The full name the assembly file for this user. </value>
    public string? ThisUserAssemblyFilePath { get; }

    #endregion
}
