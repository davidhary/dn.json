namespace cc.isr.Json.AppSettings.Services.Extensions;

/// <summary>   A service provider extension. </summary>
/// <remarks>   2023-04-29. </remarks>
public static class ServiceProviderExtension
{
    /// <summary>   An IServiceProvider extension method that gets required service. </summary>
    /// <remarks>   2023-04-29. </remarks>
    /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
    ///                                                 invalid. </exception>
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    /// <param name="serviceProvider">  The serviceProvider to act on. </param>
    /// <returns>   The required service. </returns>
    public static T GetRequiredService<T>( this IServiceProvider serviceProvider )
    {
        return serviceProvider.GetService( typeof( T ) ) is T service
            ? service
            : throw new InvalidOperationException( $"Service {typeof( T ).Name} not found." );
    }
}
