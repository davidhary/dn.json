using cc.isr.Json.AppSettings.ViewModels.Bases;

namespace cc.isr.Json.AppSettings.Services;
/// <summary>
/// This is just an example how to implement the communication between the FormsController and
/// the View (the Form).
/// </summary>
/// <remarks>   2023-04-29. </remarks>
public interface IDialogService
{
    /// <summary>   Sets marshalling context. </summary>
    /// <param name="syncContext">  Context for the synchronize. </param>
    void SetMarshallingContext( object syncContext );

    /// <summary>   Shows the modal asynchronously. </summary>
    /// <param name="registeredController"> The registered controller. </param>
    /// <returns>   The show modal. </returns>
    Task<string> ShowModalAsync( ModalViewModelBase registeredController );

    /// <summary>   Shows the message box asynchronously. </summary>
    /// <param name="title">    The title. </param>
    /// <param name="heading">  The heading. </param>
    /// <param name="message">  The message. </param>
    /// <param name="buttons">  A variable-length parameters list containing buttons. </param>
    /// <returns>   The show message box. </returns>
    Task<string> ShowMessageBoxAsync( string title, string heading, string message, params string[] buttons );
}
